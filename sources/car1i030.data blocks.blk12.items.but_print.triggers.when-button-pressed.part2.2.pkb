﻿  -- NICE PLATE CERTIFICATE WEBSERVICE PART 2/2 (BEGIN)

    if(checkbox_checked('blk1.chk_chg_plate_flag') or
      checkbox_checked('blk1.chk_alter_flag') or
      checkbox_checked('blk1.chk_trf_flag') or
      (checkbox_checked('blk1.chk_imf_flag') and checkbox_checked('blk1.chk_addr_flag')) or
      checkbox_checked('blk1.chk_nouse_flag')) then
	  
      if (NICEPLATE.pm_token is not null) then
        ret_dpr := NICEPLATE.f_prepare_data(NICEPLATE.PM_TOKEN,NICEPLATE.plate_chars,NICEPLATE.plate_num,2);
        NICEPLATE.p_notify_datasave(ret_dpr,2);
      end if; 
	  
	  if (NICEPLATE.pm_token2 is not null) then
        ret_dpr2 := NICEPLATE.f_prepare_data(NICEPLATE.PM_TOKEN2,NICEPLATE.plate_chars2,NICEPLATE.plate_num2,2);
        NICEPLATE.p_notify_datasave(ret_dpr2,2);
      end if; 
	  
	  if (NICEPLATE.pm_token3 is not null) then
        ret_dpr3 := NICEPLATE.f_prepare_data(NICEPLATE.PM_TOKEN3,NICEPLATE.plate_chars3,NICEPLATE.plate_num3,2);
        NICEPLATE.p_notify_datasave(ret_dpr3,2);
      end if; 
	  
    end if;

    set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_false);
    if ((NICEPLATE.pm_token is not null) and (NICEPLATE.pm_print_url is not null)) then
      set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_true);
      NICEPLATE.pm_formlock := 1;
    end if;

	if ((NICEPLATE.pm_token2 is not null) and (NICEPLATE.pm_print_url2 is not null)) then
      set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_true);
      NICEPLATE.pm_formlock := 1;
    end if;

	if ((NICEPLATE.pm_token3 is not null) and (NICEPLATE.pm_print_url3 is not null)) then
      set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_true);
      NICEPLATE.pm_formlock := 1;
    end if; 

  -- NICE PLATE CERTIFICATE WEBSERVICE PART 2/2 (END)