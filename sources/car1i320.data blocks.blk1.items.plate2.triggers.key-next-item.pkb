﻿  -- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (BEGIN)

  NICEPLATE.att_off_code := :parameter.usr_off_Code;
  NICEPLATE.att_br_code := :parameter.usr_br_Code;
  --NICEPLATE.att_pc_no := :blk3.pc_no;
  NICEPLATE.att_car_type := :blk1.type;
  NICEPLATE.att_user_code := :global.usercode;
  NICEPLATE.att_but_printbid_name := 'blk1.but_printbid';
  NICEPLATE.att_but_printbid_blk_name := 'blk1';

  NICEPLATE.plate_chars := :blk1.plate1;
  NICEPLATE.plate_num := :blk1.plate2;

  NICEPLATE.QueryCurrentProvince;
  NICEPLATE.QueryCurrentOfficer;

  NICEPLATE.p_chk_lic(1);

  -- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (END)
