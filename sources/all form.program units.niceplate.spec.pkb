﻿-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (BEGIN)

PACKAGE NICEPLATE IS
 
  PM_FORMLOCK varchar2(3000);
  PM_TOKEN varchar2(3000);
  PM_TOKEN2 varchar2(3000);
  PM_TOKEN3 varchar2(3000);
  PM_ALERT_MSG varchar2(3000);
  PM_ALERT_MSG2 varchar2(3000);
  PM_ALERT_MSG3 varchar2(3000);
  PM_ACTION_URL varchar2(3000);
  PM_ACTION_URL2 varchar2(3000);
  PM_ACTION_URL3 varchar2(3000);
  PM_PRINT_URL varchar2(3000);
  PM_PRINT_URL2 varchar2(3000);
  PM_PRINT_URL3 varchar2(3000);
  
  att_off_code varchar2(10);
  att_br_code varchar2(10);
  att_pc_no varchar2(10);
  att_car_type varchar2(10);
  att_user_code varchar2(10);
  att_but_printbid_name varchar2(50);
  att_but_printbid_blk_name varchar2(50);
  
  plate_chars varchar2(3000);
  plate_chars2 varchar2(3000);
  plate_chars3 varchar2(3000);
  plate_num varchar2(3000);
  plate_num2 varchar2(3000);
  plate_num3 varchar2(3000);
  
  licenseProvince varchar2(300);
  
  office_titleCode varchar2(3000);
  officer_titleName varchar2(3000);
  officer_fname varchar2(3000);
  officer_lname varchar2(3000);
  
  c_rcp_no varchar2(3);
  c_brn_code varchar2(3);
  c_brn_desc varchar2(3000);
  c_status_code varchar2(2);
  c_hold_flag varchar2(1);
  c_num_body varchar2(30);
  
  owrEvidCode varchar2(3000);
  owrEvidDesc varchar2(3000);
  owrEvidProvCode varchar2(3000);
  owrEvidAmphCode varchar2(3000);
  owrEvidDocYear varchar2(3000);
  owrEvidDocNo varchar2(3000);
  owrSeq varchar2(3000);
  owrDspPrvDesc varchar2(3000);
  owrDspAmpDesc varchar2(3000);
  owrTitleCode varchar2(3000);
  owrTitleDesc varchar2(3000);
  owrFirstName varchar2(3000);
  owrLastName varchar2(3000);
  owrBirthDate varchar2(3000);
  owrNationalityCode varchar2(3000);
  owrNationalityDesc varchar2(3000);
  owrMobilePhone varchar2(3000);
  owrAddress varchar2(3000);
  owrDistCode varchar2(3000);
  owrAmphCode varchar2(3000);
  owrProvCode varchar2(3000);
  owrProvAmphDistDesc varchar2(3000);
  owrPostalCode varchar2(3000);
  
  lglEvidCode varchar2(3000);
  lglEvidDesc varchar2(3000);
  lglEvidProvCode varchar2(3000);
  lglEvidAmphCode varchar2(3000);
  lglEvidDocYear varchar2(3000);
  lglEvidDocNo varchar2(3000);
  lglSeq varchar2(3000);
  lglDspPrvDesc varchar2(3000);
  lglDspAmpDesc varchar2(3000);
  lglTitleCode varchar2(3000);
  lglTitleDesc varchar2(3000);
  lglFirstName varchar2(3000);
  lglLastName varchar2(3000);
  lglBirthDate varchar2(3000);
  lglNationalityCode varchar2(3000);
  lglNationalityDesc varchar2(3000);
  lglMobilePhone varchar2(3000);
  lglAddress varchar2(3000);
  lglDistCode varchar2(3000);
  lglAmphCode varchar2(3000);
  lglProvCode varchar2(3000);
  lglProvAmphDistDesc varchar2(3000);
  lglPostalCode varchar2(3000);

  PROCEDURE QueryCurrentProvince;
  PROCEDURE QueryCurrentOfficer;
  
  PROCEDURE QueryPersonFromPlate(p_plate1 varchar2,p_plate2 varchar2);
  PROCEDURE QueryCarDataByPlate(p_plate1 varchar2,p_plate2 varchar2);
  PROCEDURE QueryCarBrandByCode(p_brn_code varchar2);
  
  FUNCTION f_ascii_to_text(textAscii in varchar2) RETURN varchar2;
  FUNCTION f_text_to_ascii(text in varchar2) RETURN varchar2;
  
  PROCEDURE p_chk_lic (plate_set number);
  PROCEDURE p_chk_proceed (token in varchar2);
  FUNCTION f_prepare_data (token varchar2 ,plate_chars in varchar2 ,plate_num in varchar2 ,phase in number) RETURN ora_java.jobject;
  PROCEDURE p_notify_datasave(dpreq in ora_java.jobject , phase in number);
  PROCEDURE p_notify_datasave2(dpreq in ora_java.jobject , phase in number);
  PROCEDURE p_notify_datasave3(dpreq in ora_java.jobject , phase in number);
  PROCEDURE p_clear_variable;
  
END;

-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (END)