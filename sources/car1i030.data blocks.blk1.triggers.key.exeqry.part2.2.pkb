﻿    -- NICE PLATE CERTIFICATE WEBSERVICE PART 1/2 (BEGIN)

    NICEPLATE.att_off_code := :parameter.usr_off_Code;
    NICEPLATE.att_br_code := :parameter.usr_br_Code;
    NICEPLATE.att_pc_no := :blk1.txt_pc_no;
    NICEPLATE.att_car_type := :blk1.txt_type;
    NICEPLATE.att_user_code := :global.usercode;
    NICEPLATE.att_but_printbid_name := 'blk1.but_printbid';
    NICEPLATE.att_but_printbid_blk_name := 'blk1';

    NICEPLATE.plate_chars := :blk1.txt_plate1;
    NICEPLATE.plate_num := :blk1.txt_plate2;

    NICEPLATE.QueryCurrentProvince;
    NICEPLATE.QueryCurrentOfficer;

    NICEPLATE.p_chk_lic(1);

    if(checkbox_checked('blk1.CHK_TRF_FLAG')) then
      al_ret := show_alert(find_alert('AL_TRANSFER'));
      if(al_ret = 1) then
        NICEPLATE.p_clear_variable;
        clear_form(no_validate,full_rollback);
      end if;
    end if;
    
    -- NICE PLATE CERTIFICATE WEBSERVICE PART 1/2 (END)