﻿-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (BEGIN)

PACKAGE BODY NICEPLATE IS

PROCEDURE QueryPersonFromPlate(p_plate1 varchar2,p_plate2 varchar2) IS
  BEGIN
    
  owrEvidCode := null;
  owrEvidDesc := null;
  owrEvidProvCode := null;
  owrEvidAmphCode := null;
  owrEvidDocYear := null;
  owrEvidDocNo := null;
  owrSeq := null;
  owrDspPrvDesc := null;
  owrDspAmpDesc := null;
  owrTitleCode := null;
  owrTitleDesc := null;
  owrFirstName := null;
  owrLastName := null;
  owrBirthDate := null;
  owrNationalityCode := null;
  owrNationalityDesc := null;
  owrMobilePhone := null;
  owrAddress := null;
  owrDistCode := null;
  owrAmphCode := null;
  owrProvCode := null;
  owrProvAmphDistDesc := null;
  owrPostalCode := null;
  
  lglEvidCode :=null;
  lglEvidDesc :=null;
  lglEvidProvCode :=null;
  lglEvidAmphCode :=null;
  lglEvidDocYear :=null;
  lglEvidDocNo :=null;
  lglSeq :=null;
  lglDspPrvDesc :=null;
  lglDspAmpDesc :=null;
  lglTitleCode :=null;
  lglTitleDesc :=null;
  lglFirstName :=null;
  lglLastName :=null;
  lglBirthDate :=null;
  lglNationalityCode :=null;
  lglNationalityDesc :=null;
  lglMobilePhone :=null;
  lglAddress :=null;
  lglDistCode :=null;
  lglAmphCode :=null;
  lglProvCode :=null;
  lglProvAmphDistDesc :=null;
  lglPostalCode :=null;
    
  /*
  --ALL FIELD
  owrEvidCode,owrEvidDesc,owrEvidProvCode,owrEvidAmphCode,
  owrEvidDocYear,owrEvidDocNo,owrSeq,owrDspPrvDesc,owrDspAmpDesc,
  owrTitleCode,owrTitleDesc,owrFirstName,owrLastName,
  owrBirthDate,owrNationalityCode,owrNationalityDesc,owrMobilePhone,
  owrAddress,owrDistCode,owrAmphCode,owrProvCode,
  owrProvAmphDistDesc,owrPostalCode
  
  --MUST QUERY FIELD
  owrEvidDesc,
  owrDspPrvDesc,owrDspAmpDesc,
  owrTitleDesc,
  owrNationalityDesc,
  owrProvAmphDistDesc,
  */
  
  --query owner
  begin
    select
      PERSON_CODE,PRV_CODE,AMP_CODE,
      DOC_YEAR,DOC_NO,SEQ,
      TITLE_CODE,FNAME,LNAME,
      BIRTH_DATE,NAT_CODE,PHONE_MOBILE,
      ADDR,DIST_CODE,PERSON_AMP_CODE,PERSON_PRV_CODE,
      ZIP_CODE
    into
      owrEvidCode,owrEvidProvCode,owrEvidAmphCode,
      owrEvidDocYear,owrEvidDocNo,owrSeq,
      owrTitleCode,owrFirstName,owrLastName,
      owrBirthDate,owrNationalityCode,owrMobilePhone,
      owrAddress,owrDistCode,owrAmphCode,owrProvCode,
      owrPostalCode
    from car_per_combine
    where
      off_code = att_off_code and
      br_code  = att_br_code  and
      type     = att_car_type     and
      plate1   = p_plate1   and
      plate2   = p_plate2   and
      own_flag = '2';
  exception when no_data_found then
      owrEvidCode:=null;
      owrEvidProvCode:=null;
      owrEvidAmphCode:=null;
      owrEvidDocYear:=null;
      owrEvidDocNo:=null;
      owrSeq:=null;
      owrTitleCode:=null;
      owrFirstName:=null;
      owrLastName:=null;
      owrBirthDate:=null;
      owrNationalityCode:=null;
      owrMobilePhone:=null;
      owrAddress:=null;
      owrDistCode:=null;
      owrAmphCode:=null;
      owrProvCode:=null;
      owrPostalCode:=null;
  end;
  
  begin
    select doc_desc into owrEvidDesc from tab_doc
    where doc_type = to_char(to_number(owrEvidCode));
    exception when no_data_found then
      owrEvidCode := null;
      owrEvidDesc := null;
  end;
  
  begin
    select prv_desc into owrDspPrvDesc from tab_province
    where prv_code = owrEvidProvCode;
    exception when no_data_found then
      owrDspPrvDesc := null;
  end;
  
  begin
    select amp_desc into owrDspAmpDesc from tab_amphur
    where prv_code = owrEvidProvCode and amp_code = owrEvidAmphCode;
    exception when no_data_found then
      owrDspAmpDesc := null;
  end;
  
  begin
    select title_desc into owrTitleDesc from tab_title
    where title_code = owrTitleCode;
    exception when no_data_found then
      owrTitleCode := null;
      owrTitleDesc := null;
  end;
  
  begin
    select owrNationalityDesc into owrNationalityDesc from tab_national
    where nat_code = owrNationalityCode;
    exception when no_data_found then
      owrNationalityCode := null;
      owrNationalityDesc := null;
  end;
  owrProvAmphDistDesc := trim(owrDspPrvDesc)||' '||trim(owrDspAmpDesc);
  
  
  
  --query legal
  begin
    select
      PERSON_CODE,PRV_CODE,AMP_CODE,
      DOC_YEAR,DOC_NO,SEQ,
      TITLE_CODE,FNAME,LNAME,
      BIRTH_DATE,NAT_CODE,PHONE_MOBILE,
      ADDR,DIST_CODE,PERSON_AMP_CODE,PERSON_PRV_CODE,
      ZIP_CODE
    into
      lglEvidCode,lglEvidProvCode,lglEvidAmphCode,
      lglEvidDocYear,lglEvidDocNo,lglSeq,
      lglTitleCode,lglFirstName,lglLastName,
      lglBirthDate,lglNationalityCode,lglMobilePhone,
      lglAddress,lglDistCode,lglAmphCode,lglProvCode,
      lglPostalCode
    from car_per_combine
    where
      off_code = att_off_code and
      br_code  = att_br_code  and
      type     = att_car_type     and
      plate1   = p_plate1   and
      plate2   = p_plate2   and
      own_flag = '1';
  exception when no_data_found then
      lglEvidCode:=null;
      lglEvidProvCode:=null;
      lglEvidAmphCode:=null;
      lglEvidDocYear:=null;
      lglEvidDocNo:=null;
      lglSeq:=null;
      lglTitleCode:=null;
      lglFirstName:=null;
      lglLastName:=null;
      lglBirthDate:=null;
      lglNationalityCode:=null;
      lglMobilePhone:=null;
      lglAddress:=null;
      lglDistCode:=null;
      lglAmphCode:=null;
      lglProvCode:=null;
      lglPostalCode:=null;
  end;
  
  begin
    select doc_desc into lglEvidDesc from tab_doc
    where doc_type = to_char(to_number(lglEvidCode));
    exception when no_data_found then
      lglEvidCode := null;
      lglEvidDesc := null;
  end;
  
  begin
    select prv_desc into lglDspPrvDesc from tab_province
    where prv_code = lglEvidProvCode;
    exception when no_data_found then
      lglDspPrvDesc := null;
  end;
  
  begin
    select amp_desc into lglDspAmpDesc from tab_amphur
    where prv_code = lglEvidProvCode and amp_code = lglEvidAmphCode;
    exception when no_data_found then
      lglDspAmpDesc := null;
  end;
  
  begin
    select title_desc into lglTitleDesc from tab_title
    where title_code = lglTitleCode;
    exception when no_data_found then
      lglTitleCode := null;
      lglTitleDesc := null;
  end;
  
  begin
    select lglNationalityDesc into lglNationalityDesc from tab_national
    where nat_code = lglNationalityCode;
    exception when no_data_found then
      lglNationalityCode := null;
      lglNationalityDesc := null;
  end;
  lglProvAmphDistDesc := trim(lglDspPrvDesc)||' '||trim(lglDspAmpDesc);
  
END;

PROCEDURE QueryCurrentOfficer IS
  BEGIN
  
    officer_titleCode := null;
    officer_titleName := null;
    officer_fname := null;
    officer_lname := null;
  begin
    select nvl(b.title_desc,''),nvl(a.user_fname,''),nvl(a.user_lname,''),a.title_code
    into   officer_titlename,officer_fname,officer_lname,officer_titleCode
    from   ctltemp a,tab_title b
    where  a.title_code = b.title_code
      and    a.user_code  = att_user_code;
    exception when no_data_found then
      officer_titleCode := null;
      officer_titlename := null;
      officer_fname := null;
      officer_lname := null;
  end;
  
END;

PROCEDURE QueryCurrentProvince IS
  BEGIN
  
  licenseProvince := null;
  
  begin
    select prv_desc into licenseProvince from tab_province
    where prv_code = att_off_code;
    exception when no_data_found then
      licenseProvince := null;
  end;
  
END;

PROCEDURE QueryCarDataByPlate(p_plate1 varchar2,p_plate2 varchar2) IS
  BEGIN
  
    c_brn_code := null;
    c_brn_desc := null;
    c_status_code := null;
    c_hold_flag := null;
  c_num_body := null;
  begin
    select status_code,hold_flag,brn_code,num_body
    into c_status_code,c_hold_flag,c_brn_code,c_num_body
    from car_mas    
    where  off_code = att_off_code
      and    br_code  = att_br_code
      and    type     = att_car_type
      and    plate1   = p_plate1
      and    plate2   = p_plate2;
    exception when no_data_found then
      c_brn_code := null;
      c_brn_desc := null;
      c_status_code := null;
      c_hold_flag := null;
    c_num_body := null;
  end;

  QueryCarBrandByCode(c_brn_code);
END;

PROCEDURE QueryCarBrandByCode(p_brn_code varchar2) IS
   BEGIN
   
    c_brn_desc := null;
  begin
    select brn_desc
    into   c_brn_desc
    from   tab_brand
    where  brn_code = p_brn_code;
    exception when no_data_found then
      c_brn_desc := null;
  end; 
  
END;

FUNCTION f_ascii_to_text(textAscii in varchar2) RETURN varchar2 IS
  text varchar2(3000);
BEGIN
  
  if(textAscii is not null) then
    for i in 0 .. length(textAscii)/3
    loop
      text := text||CHR(substr(textAscii,(i*3)+1+i,3));
    end loop;
  else
      text:='';
  end if;
  
  return text;
  
exception when others then
  message(sqlerrm);
END;

FUNCTION f_text_to_ascii(text in varchar2) RETURN varchar2 IS
  charStr char(1);
  charAscii number;
  charArrayAsTxt varchar2(3000);
BEGIN  
  for i in 1 .. length(text)
  loop
    charStr := substr(text, i, 1 );
    charAscii := ASCII(charStr);
    if(not i=1) then
      charArrayAsTxt := charArrayAsTxt||',';
    end if;
    charArrayAsTxt := charArrayAsTxt||charAscii;
  end loop;
  return charArrayAsTxt;
  
exception when others then
  message(sqlerrm);
END;

PROCEDURE p_chk_lic (plate_set number) IS
  jo ora_java.jobject;
  rv ora_java.jobject;
  ex ora_java.jobject;
  rvb boolean;
  al_id alert;
  al_ret number;
begin
	
if (plate_set = 1 and (NICEPLATE.plate_chars is not null) and (NICEPLATE.plate_num  is not null) and (NICEPLATE.licenseProvince is not null)) then  
  jo := LicRight2DLTServiceStub.new;
  LicRight2DLTServiceStub.setServiceURL(jo,:parameter.pm_endpoint_url);

  rv := LicRight2DLTServiceStub.CheckLicense (jo,:parameter.pm_formcode,
    NICEPLATE.f_text_to_ascii(NICEPLATE.plate_chars),
	NICEPLATE.f_text_to_ascii(NICEPLATE.plate_num),
	NICEPLATE.f_text_to_ascii(NICEPLATE.licenseProvince),
	NICEPLATE.att_off_code,
	NICEPLATE.att_br_code,
	NICEPLATE.att_pc_no);
	
  NICEPLATE.pm_token := CheckLicenseResponse.getToken(rv);
  NICEPLATE.pm_alert_msg := NICEPLATE.f_ascii_to_text(CheckLicenseResponse.getAlertMessage(rv));
  NICEPLATE.pm_action_url := CheckLicenseResponse.getActionURL(rv);
  NICEPLATE.pm_print_url := CheckLicenseResponse.getPrintURL(rv);
  set_item_property(NICEPLATE.att_but_printbid_name,ENABLED ,PROPERTY_FALSE);
  
  if (NICEPLATE.pm_alert_msg is not null) then
    al_id := find_alert('AL_LIC');
    set_alert_property(al_id,ALERT_MESSAGE_TEXT,NICEPLATE.pm_alert_msg);
    al_ret := show_alert(al_id);
  end if;

  if (NICEPLATE.pm_token is not null) then
    if (NICEPLATE.pm_action_url is not null) then
      al_id := find_alert('AL_LIC');
      set_alert_property(al_id,ALERT_MESSAGE_TEXT,'¡ÃØ³Ò¡ÃÍ¡àÅ¢¤ØÁË¹Ñ§Ê×ÍÏ ¡èÍ¹´Óà¹Ô¹¡ÒÃµèÍ');
      al_ret := show_alert(al_id);
	  
      web.show_document(NICEPLATE.pm_action_url);
	  
      al_id := find_alert('AL_LIC');
      set_alert_property(al_id,ALERT_MESSAGE_TEXT,'`àÁ×èÍ¡ÃÍ¡àÅ¢¤ØÁË¹Ñ§Ê×ÍÊÓ¤Ñ­Ï àÊÃç¨áÅéÇ¡ÃØÃ³Ò¡´»ØèÁµ¡Å§');
      al_ret := show_alert(al_id);
    end if;
	
    -- Check Proceed Allow Here
    NICEPLATE.p_chk_proceed(NICEPLATE.pm_token);
	
    if (NICEPLATE.pm_print_url is not null) then
      set_item_property(NICEPLATE.att_but_printbid_name,ENABLED ,PROPERTY_TRUE);
      web.show_document(NICEPLATE.pm_print_url);
    end if;
  end if;
end if;

if (plate_set = 2 and (NICEPLATE.plate_chars2 is not null) and (NICEPLATE.plate_num2  is not null) and (NICEPLATE.licenseProvince is not null)) then  
  jo := LicRight2DLTServiceStub.new;
  LicRight2DLTServiceStub.setServiceURL(jo,:parameter.pm_endpoint_url);

  rv := LicRight2DLTServiceStub.CheckLicense (jo,:parameter.pm_formcode,
    NICEPLATE.f_text_to_ascii(NICEPLATE.plate_chars2),
	NICEPLATE.f_text_to_ascii(NICEPLATE.plate_num2),
	NICEPLATE.f_text_to_ascii(NICEPLATE.licenseProvince),
	NICEPLATE.att_off_code,
	NICEPLATE.att_br_code,
	NICEPLATE.att_pc_no);
	
  NICEPLATE.pm_token2 := CheckLicenseResponse.getToken(rv);
  NICEPLATE.pm_alert_msg2 := NICEPLATE.f_ascii_to_text(CheckLicenseResponse.getAlertMessage(rv));
  NICEPLATE.pm_action_url2 := CheckLicenseResponse.getActionURL(rv);
  NICEPLATE.pm_print_url2 := CheckLicenseResponse.getPrintURL(rv);
  set_item_property(NICEPLATE.att_but_printbid_name,ENABLED ,PROPERTY_FALSE);
  
  if (NICEPLATE.pm_alert_msg2 is not null) then
    al_id := find_alert('AL_LIC');
    set_alert_property(al_id,ALERT_MESSAGE_TEXT,NICEPLATE.pm_alert_msg2);
    al_ret := show_alert(al_id);
  end if;

  if (NICEPLATE.pm_token2 is not null) then
    if (NICEPLATE.pm_action_url2 is not null) then
      al_id := find_alert('AL_LIC');
      set_alert_property(al_id,ALERT_MESSAGE_TEXT,'¡ÃØ³Ò¡ÃÍ¡àÅ¢¤ØÁË¹Ñ§Ê×ÍÏ ¡èÍ¹´Óà¹Ô¹¡ÒÃµèÍ');
      al_ret := show_alert(al_id);
	  
      web.show_document(NICEPLATE.pm_action_url2);
	  
      al_id := find_alert('AL_LIC');
      set_alert_property(al_id,ALERT_MESSAGE_TEXT,'`àÁ×èÍ¡ÃÍ¡àÅ¢¤ØÁË¹Ñ§Ê×ÍÊÓ¤Ñ­Ï àÊÃç¨áÅéÇ¡ÃØÃ³Ò¡´»ØèÁµ¡Å§');
      al_ret := show_alert(al_id);
    end if;
	
    -- Check Proceed Allow Here
    NICEPLATE.p_chk_proceed(NICEPLATE.pm_token2);
	
    if (NICEPLATE.pm_print_url2 is not null) then
      web.show_document(NICEPLATE.pm_print_url2);
    end if;
  end if;
end if;

if (plate_set = 3 and (NICEPLATE.plate_chars3 is not null) and (NICEPLATE.plate_num3  is not null) and (NICEPLATE.licenseProvince is not null)) then  
  jo := LicRight2DLTServiceStub.new;
  LicRight2DLTServiceStub.setServiceURL(jo,:parameter.pm_endpoint_url);

  rv := LicRight2DLTServiceStub.CheckLicense (jo,:parameter.pm_formcode,
    NICEPLATE.f_text_to_ascii(NICEPLATE.plate_chars3),
	NICEPLATE.f_text_to_ascii(NICEPLATE.plate_num3),
	NICEPLATE.f_text_to_ascii(NICEPLATE.licenseProvince),
	NICEPLATE.att_off_code,
	NICEPLATE.att_br_code,
	NICEPLATE.att_pc_no);
	
  NICEPLATE.pm_token3 := CheckLicenseResponse.getToken(rv);
  NICEPLATE.pm_alert_msg3 := NICEPLATE.f_ascii_to_text(CheckLicenseResponse.getAlertMessage(rv));
  NICEPLATE.pm_action_url3 := CheckLicenseResponse.getActionURL(rv);
  NICEPLATE.pm_print_url3 := CheckLicenseResponse.getPrintURL(rv);
  set_item_property(NICEPLATE.att_but_printbid_name,ENABLED ,PROPERTY_FALSE);
  
  if (NICEPLATE.pm_alert_msg3 is not null) then
    al_id := find_alert('AL_LIC');
    set_alert_property(al_id,ALERT_MESSAGE_TEXT,NICEPLATE.pm_alert_msg3);
    al_ret := show_alert(al_id);
  end if;

  if (NICEPLATE.pm_token3 is not null) then
    if (NICEPLATE.pm_action_url3 is not null) then
      al_id := find_alert('AL_LIC');
      set_alert_property(al_id,ALERT_MESSAGE_TEXT,'¡ÃØ³Ò¡ÃÍ¡àÅ¢¤ØÁË¹Ñ§Ê×ÍÏ ¡èÍ¹´Óà¹Ô¹¡ÒÃµèÍ');
      al_ret := show_alert(al_id);
	  
      web.show_document(NICEPLATE.pm_action_url3);
	  
      al_id := find_alert('AL_LIC');
      set_alert_property(al_id,ALERT_MESSAGE_TEXT,'`àÁ×èÍ¡ÃÍ¡àÅ¢¤ØÁË¹Ñ§Ê×ÍÊÓ¤Ñ­Ï àÊÃç¨áÅéÇ¡ÃØÃ³Ò¡´»ØèÁµ¡Å§');
      al_ret := show_alert(al_id);
    end if;
	
    -- Check Proceed Allow Here
    NICEPLATE.p_chk_proceed(NICEPLATE.pm_token3);
	
    if (NICEPLATE.pm_print_url3 is not null) then
      web.show_document(NICEPLATE.pm_print_url3);
    end if;
  end if;
end if;

exception
  when ORA_JAVA.JAVA_ERROR then 
    message(' Unable to call out to Java, '||ORA_JAVA.LAST_ERROR ); 
  when ORA_JAVA.EXCEPTION_THROWN then 
    ex := ORA_JAVA.LAST_EXCEPTION; 
    message(OBJECT.toString(ex)); 
end;

PROCEDURE p_chk_proceed(token varchar2) IS
  jo ora_java.jobject;
  rv ora_java.jobject;
  ex ora_java.jobject;
  dpreq ora_java.jobject;
  dpres ora_java.jobject;
  ndsres ora_java.jobject;
  rvb boolean;
  al_id alert;
  al_ret number;
begin

if (token is not null) then
  -- Check Proceed Allow
  jo := LicRight2DLTServiceStub.new;
  LicRight2DLTServiceStub.setServiceURL(jo,:parameter.pm_endpoint_url);
  
  rvb := LicRight2DLTServiceStub.CheckProceedAllowed(jo,token);
  if (rvb) then
    null;
  else
    al_id := find_alert('AL_LIC');
    set_alert_property(al_id,ALERT_MESSAGE_TEXT,'äÁèÊÒÁÒÃ¶µÓà¹Ô¹¡ÒÃµèÍä´é ¿ÃÍÁ¨Ð¶Ù¡ÅéÒ§·Ñé§ËÁ´');
    al_ret := show_alert(al_id);
    -- clear form
    NICEPLATE.p_clear_variable;
    clear_form(no_validate,full_rollback);
    rollback;
  end if;
end if;

exception
  when ORA_JAVA.JAVA_ERROR then 
    message(' Unable to call out to Java, '||ORA_JAVA.LAST_ERROR ); 
  when ORA_JAVA.EXCEPTION_THROWN then 
    ex := ORA_JAVA.LAST_EXCEPTION; 
    message(OBJECT.toString(ex)); 
end;

FUNCTION f_prepare_data (token varchar2 ,plate_chars in varchar2 ,plate_num in varchar2 ,phase in number) RETURN ora_java.jobject IS
  jo ora_java.jobject; 
  rv ora_java.jobject; 
  ex ora_java.jobject;
  dpreq ora_java.jobject;
  dpres ora_java.jobject;
  ndsres ora_java.jobject;
  rvb boolean; 
  al_id alert;
  al_ret number;
begin
  
if (token is not null) then
  NICEPLATE.pm_formlock := 1;
  
  NICEPLATE.QueryCarDataByPlate(plate_chars,plate_num);
  NICEPLATE.QueryPersonFromPlate(plate_chars,plate_num);
  
  -- Prepare Data
  dpreq := DPR.new;
  DPR.setLicenseCharacters(dpreq,NICEPLATE.f_text_to_ascii(plate_chars));
  DPR.setLicenseNumber(dpreq,NICEPLATE.f_text_to_ascii(plate_num));
  DPR.setLicenseProv(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.licenseProvince));
  DPR.setPhaseNumber(dpreq,phase);
  
  if (NICEPLATE.c_rcp_no is not null) then
    DPR.setReceiptNumber(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.c_rcp_no));
  end if;
  if (NICEPLATE.c_brn_code is not null) then
    DPR.setVehicleBrandCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.c_brn_code));
  end if;
  if (NICEPLATE.c_brn_desc is not null) then
    DPR.setVehicleBrandDesc(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.c_brn_desc));
  end if;
  if (NICEPLATE.c_num_body is not null) then
    DPR.setVehicleIdNumber(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.c_num_body));
  end if;
    
  DPR.setHasSequestrated(dpreq,NICEPLATE.c_hold_flag='H');
  DPR.setHasNotUsedForever(dpreq,NICEPLATE.c_status_code='PM');
  
  --office and officer data
  if (NICEPLATE.att_br_code is not null) then
    DPR.setOfficeBranchCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.att_br_code));
  end if;  
  if (NICEPLATE.att_off_code is not null) then
    DPR.setOfficeCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.att_off_code));
  end if;  
  if (NICEPLATE.licenseProvince is not null) then
    DPR.setOfficeProv(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.licenseProvince));
  end if;  
  if (NICEPLATE.att_user_code is not null) then
    DPR.setOfficerCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.att_user_code));
  end if;  
  if (NICEPLATE.officer_fname is not null) then
    DPR.setOfficerFirstName(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.officer_fname));
  end if;  
  if (NICEPLATE.officer_lname is not null) then
    DPR.setOfficerLastName(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.officer_lname));
  end if;
  if (NICEPLATE.att_pc_no is not null) then
    DPR.setOfficerPcCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.att_pc_no));
  end if;   
  if (NICEPLATE.officer_titleCode is not null) then
    DPR.setOfficerTitleCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.officer_titleCode));
  end if;  
  if (NICEPLATE.officer_titleName is not null) then
    DPR.setOfficerTitleDesc(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.officer_titleName));
  end if;
  
  --set legal person data
  if (NICEPLATE.lglAddress is not null) then
    DPR.setLglAddress(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglAddress));
  end if;  
  if (NICEPLATE.lglAmphCode is not null) then
    DPR.setLglAmpheCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglAmphCode));
  end if;
  if (NICEPLATE.lglBirthDate is not null) then
    DPR.setLglBirthDate(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglBirthDate));
  end if;
  if (NICEPLATE.lglDistCode is not null) then
    DPR.setLglDistCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglDistCode));
  end if;
  if (NICEPLATE.lglDspAmpDesc is not null) then
    DPR.setLglDspAmpDesc(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglDspAmpDesc));
  end if;
  if (NICEPLATE.lglDspPrvDesc is not null) then
    DPR.setLglDspPrvDesc(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglDspPrvDesc));
  end if;
  if (NICEPLATE.lglEvidAmphCode is not null) then
    DPR.setLglEvidAmpheCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglEvidAmphCode));
  end if;
  if (NICEPLATE.lglEvidCode is not null) then
    DPR.setLglEvidCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglEvidCode));
  end if;
  if (NICEPLATE.lglEvidDesc is not null) then
    DPR.setLglEvidDesc(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglEvidDesc));
  end if;
  if (NICEPLATE.lglEvidDocNo is not null) then
    DPR.setLglEvidDocNo(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglEvidDocNo));
  end if;
  if (NICEPLATE.lglEvidDocYear is not null) then
    DPR.setLglEvidDocYear(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglEvidDocYear));
  end if;
  if (NICEPLATE.lglEvidProvCode is not null) then
    DPR.setLglEvidProvCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglEvidProvCode));
  end if;
  if (NICEPLATE.lglFirstName is not null) then
    DPR.setLglFirstName(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglFirstName));
  end if;
  if (NICEPLATE.lglLastName is not null) then
    DPR.setLglLastName(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglLastName));
  end if;
  if (NICEPLATE.lglMobilePhone is not null) then
    DPR.setLglMobilePhone(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglMobilePhone));
  end if;
  if (NICEPLATE.lglNationalityCode is not null) then
    DPR.setLglNationalityCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglNationalityCode));
  end if;
  if (NICEPLATE.lglNationalityDesc is not null) then
    DPR.setLglNationalityDesc(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglNationalityDesc));
  end if;
  if (NICEPLATE.lglPostalCode is not null) then
    DPR.setLglPostalCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglPostalCode));
  end if;
  if (NICEPLATE.lglProvAmphDistDesc is not null) then
    DPR.setLglProvAmpheDistDesc(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglProvAmphDistDesc));
  end if;
  if (NICEPLATE.lglProvCode is not null) then
    DPR.setLglProvCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglProvCode));
  end if;
  if (NICEPLATE.lglSeq is not null) then
    DPR.setLglSeq(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglSeq));
  end if;
  if (NICEPLATE.lglTitleCode is not null) then
    DPR.setLglTitleCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglTitleCode));
  end if;
  if (NICEPLATE.lglTitleDesc is not null) then
    DPR.setLglTitleDesc(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.lglTitleDesc));
  end if;
  
  --set owner data
  if (NICEPLATE.owrAddress is not null) then
    DPR.setOwrAddress(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrAddress));
  end if;  
  if (NICEPLATE.owrAmphCode is not null) then
    DPR.setOwrAmpheCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrAmphCode));
  end if;
  if (NICEPLATE.owrBirthDate is not null) then
    DPR.setOwrBirthDate(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrBirthDate));
  end if;
  if (NICEPLATE.owrDistCode is not null) then
    DPR.setOwrDistCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrDistCode));
  end if;
  if (NICEPLATE.owrDspAmpDesc is not null) then
    DPR.setOwrDspAmpDesc(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrDspAmpDesc));
  end if;
  if (NICEPLATE.owrDspPrvDesc is not null) then
    DPR.setOwrDspPrvDesc(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrDspPrvDesc));
  end if;
  if (NICEPLATE.owrEvidAmphCode is not null) then
    DPR.setOwrEvidAmpheCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrEvidAmphCode));
  end if;
  if (NICEPLATE.owrEvidCode is not null) then
    DPR.setOwrEvidCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrEvidCode));
  end if;
  if (NICEPLATE.owrEvidDesc is not null) then
    DPR.setOwrEvidDesc(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrEvidDesc));
  end if;
  if (NICEPLATE.owrEvidDocNo is not null) then
    DPR.setOwrEvidDocNo(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrEvidDocNo));
  end if;
  if (NICEPLATE.owrEvidDocYear is not null) then
    DPR.setOwrEvidDocYear(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrEvidDocYear));
  end if;
  if (NICEPLATE.owrEvidProvCode is not null) then
    DPR.setOwrEvidProvCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrEvidProvCode));
  end if;
  if (NICEPLATE.owrFirstName is not null) then
    DPR.setOwrFirstName(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrFirstName));
  end if;
  if (NICEPLATE.owrLastName is not null) then
    DPR.setOwrLastName(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrLastName));
  end if;
  if (NICEPLATE.owrMobilePhone is not null) then
    DPR.setOwrMobilePhone(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrMobilePhone));
  end if;
  if (NICEPLATE.owrNationalityCode is not null) then
    DPR.setOwrNationalityCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrNationalityCode));
  end if;
  if (NICEPLATE.owrNationalityDesc is not null) then
    DPR.setOwrNationalityDesc(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrNationalityDesc));
  end if;
  if (NICEPLATE.owrPostalCode is not null) then
    DPR.setOwrPostalCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrPostalCode));
  end if;
  if (NICEPLATE.owrProvAmphDistDesc is not null) then
    DPR.setOwrProvAmpheDistDesc(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrProvAmphDistDesc));
  end if;
  if (NICEPLATE.owrProvCode is not null) then
    DPR.setOwrProvCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrProvCode));
  end if;
  if (NICEPLATE.owrSeq is not null) then
    DPR.setOwrSeq(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrSeq));
  end if;
  if (NICEPLATE.owrTitleCode is not null) then
    DPR.setOwrTitleCode(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrTitleCode));
  end if;
  if (NICEPLATE.owrTitleDesc is not null) then
    DPR.setOwrTitleDesc(dpreq,NICEPLATE.f_text_to_ascii(NICEPLATE.owrTitleDesc));
  end if;
  
  return dpreq;
else
  return null;
end if;

exception
  when ORA_JAVA.JAVA_ERROR then 
    message(' Unable to call out to Java, '||ORA_JAVA.LAST_ERROR ); 
  when ORA_JAVA.EXCEPTION_THROWN then 
    ex := ORA_JAVA.LAST_EXCEPTION; 
    message(OBJECT.toString(ex)); 
end;

PROCEDURE p_notify_datasave(dpreq in ora_java.jobject , phase in number) IS
  jo ora_java.jobject; 
  rv ora_java.jobject; 
  ex ora_java.jobject;
  dpres ora_java.jobject;
  ndsres ora_java.jobject;
  rvb boolean; 
  al_id alert;
  al_ret number;
begin

if (NICEPLATE.pm_token is not null) then
  NICEPLATE.pm_formlock := 1;

  jo := LicRight2DLTServiceStub.new;
  LicRight2DLTServiceStub.setServiceURL(jo,:parameter.pm_endpoint_url);
  dpres := LicRight2DLTServiceStub.prepareDataDPR(jo, NICEPLATE.pm_token, dpreq);
  ndsres := LicRight2DLTServiceStub.notifyDataSave(jo, NICEPLATE.pm_token);

    NICEPLATE.pm_action_url := NotifyDataSaveResponse.getActionURL(ndsres);
    NICEPLATE.pm_print_url := NotifyDataSaveResponse.getPrintURL(ndsres);

  if(phase = 2 or phase = 4) then
    if (NICEPLATE.pm_action_url is not null) then
      web.show_document(NICEPLATE.pm_action_url);
    end if;
  end if;
end if;

exception
  when ORA_JAVA.JAVA_ERROR then 
    message(' Unable to call out to Java, '||ORA_JAVA.LAST_ERROR ); 
  when ORA_JAVA.EXCEPTION_THROWN then 
    ex := ORA_JAVA.LAST_EXCEPTION; 
    message(OBJECT.toString(ex)); 
end;

PROCEDURE p_notify_datasave2(dpreq in ora_java.jobject , phase in number) IS
  jo ora_java.jobject; 
  rv ora_java.jobject; 
  ex ora_java.jobject;
  dpres ora_java.jobject;
  ndsres ora_java.jobject;
  rvb boolean; 
  al_id alert;
  al_ret number;
begin

if (NICEPLATE.pm_token2 is not null) then
  NICEPLATE.pm_formlock := 1;

  jo := LicRight2DLTServiceStub.new;
  LicRight2DLTServiceStub.setServiceURL(jo,:parameter.pm_endpoint_url);
  dpres := LicRight2DLTServiceStub.prepareDataDPR(jo, NICEPLATE.pm_token2, dpreq);
  ndsres := LicRight2DLTServiceStub.notifyDataSave(jo, NICEPLATE.pm_token2);

  NICEPLATE.pm_action_url2 := NotifyDataSaveResponse.getActionURL(ndsres);
  NICEPLATE.pm_print_url2 := NotifyDataSaveResponse.getPrintURL(ndsres);

  if(phase = 2 or phase = 4) then
    if (NICEPLATE.pm_action_url2 is not null) then
      web.show_document(NICEPLATE.pm_action_url2);
    end if;
  end if;
end if;

exception
  when ORA_JAVA.JAVA_ERROR then 
    message(' Unable to call out to Java, '||ORA_JAVA.LAST_ERROR ); 
  when ORA_JAVA.EXCEPTION_THROWN then 
    ex := ORA_JAVA.LAST_EXCEPTION; 
    message(OBJECT.toString(ex)); 
end;

PROCEDURE p_notify_datasave3(dpreq in ora_java.jobject , phase in number) IS
  jo ora_java.jobject; 
  rv ora_java.jobject; 
  ex ora_java.jobject;
  dpres ora_java.jobject;
  ndsres ora_java.jobject;
  rvb boolean; 
  al_id alert;
  al_ret number;
begin

if (NICEPLATE.pm_token3 is not null) then
  NICEPLATE.pm_formlock := 1;

  jo := LicRight2DLTServiceStub.new;
  LicRight2DLTServiceStub.setServiceURL(jo,:parameter.pm_endpoint_url);
  dpres := LicRight2DLTServiceStub.prepareDataDPR(jo, NICEPLATE.pm_token3, dpreq);
  ndsres := LicRight2DLTServiceStub.notifyDataSave(jo, NICEPLATE.pm_token3);

  NICEPLATE.pm_action_url3 := NotifyDataSaveResponse.getActionURL(ndsres);
  NICEPLATE.pm_print_url3 := NotifyDataSaveResponse.getPrintURL(ndsres);

  if(phase = 2 or phase = 4) then
    if (NICEPLATE.pm_action_url3 is not null) then
      web.show_document(NICEPLATE.pm_action_url3);
    end if;
  end if;
end if;

exception
  when ORA_JAVA.JAVA_ERROR then 
    message(' Unable to call out to Java, '||ORA_JAVA.LAST_ERROR ); 
  when ORA_JAVA.EXCEPTION_THROWN then 
    ex := ORA_JAVA.LAST_EXCEPTION; 
    message(OBJECT.toString(ex)); 
end;

PROCEDuRE p_clear_variable IS
begin
  
  if(NICEPLATE.att_but_printbid_name is not null) then
    go_block(NICEPLATE.att_but_printbid_blk_name);
    set_item_property(NICEPLATE.att_but_printbid_name,ENABLED ,PROPERTY_FALSE);
  end if;
  
  NICEPLATE.PM_FORMLOCK := null;
  NICEPLATE.PM_TOKEN := null;
  NICEPLATE.PM_TOKEN2 := null;
  NICEPLATE.PM_TOKEN3 := null;
  NICEPLATE.PM_ALERT_MSG := null;
  NICEPLATE.PM_ALERT_MSG2 := null;
  NICEPLATE.PM_ALERT_MSG3 := null;
  NICEPLATE.PM_ACTION_URL := null;
  NICEPLATE.PM_ACTION_URL2 := null;
  NICEPLATE.PM_ACTION_URL3 := null;
  NICEPLATE.PM_PRINT_URL := null;
  NICEPLATE.PM_PRINT_URL2 := null;
  NICEPLATE.PM_PRINT_URL3 := null;

  NICEPLATE.att_off_code := null;
  NICEPLATE.att_br_code := null;
  NICEPLATE.att_pc_no := null;
  NICEPLATE.att_car_type := null;
  NICEPLATE.att_user_code := null;
  NICEPLATE.att_but_printbid_name := null;

  NICEPLATE.plate_chars := null;
  NICEPLATE.plate_chars2 := null;
  NICEPLATE.plate_chars3 := null;
  NICEPLATE.plate_num := null;
  NICEPLATE.plate_num2 := null;
  NICEPLATE.plate_num3 := null;

  NICEPLATE.licenseProvince := null;

  NICEPLATE.officer_titleCode := null;
  NICEPLATE.officer_titleName := null;
  NICEPLATE.officer_fname := null;
  NICEPLATE.officer_lname := null;

  NICEPLATE.c_rcp_no := null;
  NICEPLATE.c_brn_code := null;
  NICEPLATE.c_brn_desc := null;
  NICEPLATE.c_status_code := null;
  NICEPLATE.c_hold_flag := null;
  NICEPLATE.c_num_body := null;

  NICEPLATE.owrEvidCode := null;
  NICEPLATE.owrEvidDesc := null;
  NICEPLATE.owrEvidProvCode := null;
  NICEPLATE.owrEvidAmphCode := null;
  NICEPLATE.owrEvidDocYear := null;
  NICEPLATE.owrEvidDocNo := null;
  NICEPLATE.owrSeq := null;
  NICEPLATE.owrDspPrvDesc := null;
  NICEPLATE.owrDspAmpDesc := null;
  NICEPLATE.owrTitleCode := null;
  NICEPLATE.owrTitleDesc := null;
  NICEPLATE.owrFirstName := null;
  NICEPLATE.owrLastName := null;
  NICEPLATE.owrBirthDate := null;
  NICEPLATE.owrNationalityCode := null;
  NICEPLATE.owrNationalityDesc := null;
  NICEPLATE.owrMobilePhone := null;
  NICEPLATE.owrAddress := null;
  NICEPLATE.owrDistCode := null;
  NICEPLATE.owrAmphCode := null;
  NICEPLATE.owrProvCode := null;
  NICEPLATE.owrProvAmphDistDesc := null;
  NICEPLATE.owrPostalCode := null;

  NICEPLATE.lglEvidCode := null;
  NICEPLATE.lglEvidDesc := null;
  NICEPLATE.lglEvidProvCode := null;
  NICEPLATE.lglEvidAmphCode := null;
  NICEPLATE.lglEvidDocYear := null;
  NICEPLATE.lglEvidDocNo := null;
  NICEPLATE.lglSeq := null;
  NICEPLATE.lglDspPrvDesc := null;
  NICEPLATE.lglDspAmpDesc := null;
  NICEPLATE.lglTitleCode := null;
  NICEPLATE.lglTitleDesc := null;
  NICEPLATE.lglFirstName := null;
  NICEPLATE.lglLastName := null;
  NICEPLATE.lglBirthDate := null;
  NICEPLATE.lglNationalityCode := null;
  NICEPLATE.lglNationalityDesc := null;
  NICEPLATE.lglMobilePhone := null;
  NICEPLATE.lglAddress := null;
  NICEPLATE.lglDistCode := null;
  NICEPLATE.lglAmphCode := null;
  NICEPLATE.lglProvCode := null;
  NICEPLATE.lglProvAmphDistDesc := null;
  NICEPLATE.lglPostalCode := null;
end;

END;

-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (END)