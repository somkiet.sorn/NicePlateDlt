﻿  -- NICE PLATE CERTIFICATE WEBSERVICE PART 3/3 (BEGIN)
  
  if (NICEPLATE.pm_token is not null) then
    ret_dpr := NICEPLATE.f_prepare_data(NICEPLATE.PM_TOKEN,NICEPLATE.plate_chars,NICEPLATE.plate_num,1);
    NICEPLATE.p_notify_datasave(ret_dpr,1);
  end if;
  
  if (NICEPLATE.pm_token2 is not null) then
    ret_dpr2 := NICEPLATE.f_prepare_data(NICEPLATE.PM_TOKEN2,NICEPLATE.plate_chars2,NICEPLATE.plate_num2,1);
    NICEPLATE.p_notify_datasave2(ret_dpr2,1);
  end if;
  
  if (NICEPLATE.pm_token3 is not null) then
    ret_dpr3 := NICEPLATE.f_prepare_data(NICEPLATE.PM_TOKEN3,NICEPLATE.plate_chars3,NICEPLATE.plate_num3,1);
    NICEPLATE.p_notify_datasave3(ret_dpr3,1);
  end if;
  
  -- NICE PLATE CERTIFICATE WEBSERVICE PART 3/3 (END)