﻿  -- NICE PLATE CERTIFICATE WEBSERVICE PART 2/3 (BEGIN)

  if (NICEPLATE.pm_token is not null) then
    NICEPLATE.p_chk_proceed(NICEPLATE.pm_token);
  end if;

  if (NICEPLATE.pm_token2 is not null) then
    NICEPLATE.p_chk_proceed(NICEPLATE.pm_token2);
  end if;

  if (NICEPLATE.pm_token3 is not null) then
    NICEPLATE.p_chk_proceed(NICEPLATE.pm_token3);
  end if;
  
  -- NICE PLATE CERTIFICATE WEBSERVICE PART 2/3 (END)