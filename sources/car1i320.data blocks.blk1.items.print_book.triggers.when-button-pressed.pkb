﻿		-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (BEGIN)
		
        set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_false);
        if ((NICEPLATE.pm_token is not null) and (NICEPLATE.pm_print_url is not null)) then
          set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_true);
          NICEPLATE.pm_formlock := 1;
        end if;

        if ((NICEPLATE.pm_token2 is not null) and (NICEPLATE.pm_print_url2 is not null)) then
          set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_true);
          NICEPLATE.pm_formlock := 1;
        end if;

        if ((NICEPLATE.pm_token3 is not null) and (NICEPLATE.pm_print_url3 is not null)) then
          set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_true);
          NICEPLATE.pm_formlock := 1;
        end if;
		
		-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (END)