﻿-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (BEGIN)

NICEPLATE.att_off_code := :parameter.usr_off_Code;
NICEPLATE.att_br_code := :parameter.usr_br_Code;
NICEPLATE.att_pc_no := :blk2.txt_pc_no;
NICEPLATE.att_car_type := :blk2.type;
NICEPLATE.att_user_code := :global.usercode;
NICEPLATE.att_but_printbid_name := 'blk2.but_printbid';
NICEPLATE.att_but_printbid_blk_name := 'blk2';

NICEPLATE.plate_chars := :blk2.plate1;
NICEPLATE.plate_num := :blk2.plate2;

NICEPLATE.QueryCurrentProvince;
NICEPLATE.QueryCurrentOfficer;

NICEPLATE.p_chk_lic(1);

-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (END)