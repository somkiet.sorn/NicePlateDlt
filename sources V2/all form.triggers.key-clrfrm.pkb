﻿-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (BEGIN)

declare
  qry_count number;
begin
  begin
    select count(*) into qry_count
    from DLT.CAR_OperationRequest
    where Token in (NICEPLATE.PM_TOKEN, NICEPLATE.PM_TOKEN2, NICEPLATE.PM_TOKEN3)
      and IsDone = 1 and IsPrinted = 0;
    exception when no_data_found then
      qry_count := 0;
  end;

  if (qry_count > 0) then
    P_SHOW_ALERT('AL_LIC','¡ÃØ³Ò¾ÔÁ¾ìË¹Ñ§Ê×ÍÊÓ¤Ñ­Ï ¡èÍ¹');
    go_item(NICEPLATE.att_but_printbid_name);
    return;
  end if;

  NICEPLATE.p_clear_variable;
end;

-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (END)