﻿-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (BEGIN)

--CAR1I010 => blk2.but_printbid
--CAR1I030 => blk1.but_printbid
--CAR1I150 => blk1.but_printbid
--CAR1I300 => blk1.but_printbid
--CAR1I320 => blk1.but_printbid
--CAR1I340 => blk2.but_printbid

declare
  ls_reg_flag varchar2(1);
  alert number;
  temp_rcp_no varchar2(11);
begin
  if (NICEPLATE.pm_token is not null) then
    web.show_document(NICEPLATE.PM_INIT_PRINT_URL||NICEPLATE.pm_token);
    NICEPLATE.pm_formlock := 0;
  end if;
  if (NICEPLATE.pm_token2 is not null) then
    web.show_document(NICEPLATE.PM_INIT_PRINT_URL||NICEPLATE.pm_token2);
    NICEPLATE.pm_formlock := 0;
  end if;
  if (NICEPLATE.pm_token3 is not null) then
    web.show_document(NICEPLATE.PM_INIT_PRINT_URL||NICEPLATE.pm_token3);
    NICEPLATE.pm_formlock := 0;
  end if;
  commit;
end;

-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (END)