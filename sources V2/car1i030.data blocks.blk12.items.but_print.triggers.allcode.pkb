declare
	  ln_Max         number;
	  ls_TrDate      varchar2(8);
	  ls_RcvFlag     varchar2(1);
	  ln_Count       number;
	  alert          number;
	  ln_Count2      number;
	  ls_SndDate     varchar2(8);
	  ls_CtlDate     varchar2(8);
	  ls_type 	     char(1);
	  ls_Year        varchar2(4);
	  ls_PrintFlag   varchar2(1);
	  ls_WhiteColor  varchar2(100);
	  ls_GrayColor   varchar2(100);
	  ls_FormType    varchar2(1);
	 --credit------------------------------------
	hFld ITEM := FIND_ITEM('blk12.creditcard');
	param varchar2(172);
	retParam varchar2(250); 
	ls_text_line1 varchar2(255);
	ls_text_line2 varchar2(255);
	resp_code char(2);
	com_port varchar2(5);
	batch_no varchar2(6);
	trace_invoice varchar2(6);
	terminal_id varchar2(8);
	card_no varchar2(20);
	ls_amt varchar2(18);
	ls_OffRegAbrev   varchar2(10);
	ls_pay_with varchar2(1);
	svat_rate number ;
	sbank_acq_code varchar2(2);
	sBANK_FEE_PERC number ;
	sEDC_TIME_OUT number ;
		ls_bank_code varchar2(2);
		shol_flag varchar2(1);
		ls_type_card char(1);
begin	  
   P_CHK_BEFORE_SAVE;
   p_check_rcp_srl_no;
   ln_Count := 0;
		begin
			select nvl(OFF_REG_ABREV,' ')
			into    ls_OffRegAbrev
			from    tab_office_br
			where   off_code = :parameter.usr_off_code
			and     br_code  = :parameter.usr_br_code;
	  exception when no_data_found then
	  	  ls_OffRegAbrev := ' ';
	  end;   

   
   /*--------Select Data from Fnc_Control-------*/
   begin
	 select rcv_flag,snd_date,ctl_date,hol_flag
	 into   ls_RcvFlag,ls_SndDate,ls_CtlDate,shol_flag
	 from   fnc_control
	 where  iss_off_code = :parameter.usr_off_code
	 and    iss_br_code  = :parameter.usr_br_code;
   exception when no_data_found then
  	  ls_RcvFlag := null;
  	  shol_flag := null;
	 end;
  
   /*----µÃÇ¨ÊÍºÇèÒÍÂÙèã¹¢Ñé¹µÍ¹»Ô´ÊÔé¹ÇÑ¹ÅÐäÁèä´éÃÑºËÅÑ§»Ô´ºÑ­ªÕËÃ×ÍäÁè-----*/
	 ln_Count := 0;
	 begin
	 select 1 --count(*)
	 into   ln_Count
   from   fnc_control 
   where  iss_off_code = :parameter.usr_off_code 
   and    iss_br_code  = :parameter.usr_br_code
   and    rcv_flag = 'Y'
   and    gmn_flag = 'G9'
   and rownum = 1;
	 exception when no_data_found then
		  ln_Count := 0;
	 end;
	 
	 if ln_Count > 0 then
	 	  alert := f_show_alert('äÁèÊÒÁÒÃ¶ºÑ¹·Ö¡ÃÑºà§Ô¹ ¡ÓÅÑ§»Ô´ÊÃØ»ÃÑºà§Ô¹»ÃÐ¨ÓÇÑ¹','al_stop');
	 	  raise form_trigger_failure;
	 else
	 	  :blk12.rcv_flag := ls_RcvFlag;
      :blk12.snd_date := ls_SndDate; /* ÇÑ¹·Õè¹ÓÊè§à§Ô¹ */ 
      :blk12.tr_date  := ls_CtlDate;
      :blk12.rcv_day_flag := 'N';   /* Ê¶Ò¹Ð¡ÒÃ»Ô´ÊÃØ»ÃÑºà§Ô¹»ÃÐ¨ÓÇÑ¹ */
	 end if;	
   
 
   ls_TrDate := to_char(to_number(to_char(sysdate,'yyyy'))+543)||to_char(sysdate,'mmdd');

   ---update fnc_rcp_run_ctl
   begin
	 update fnc_rcp_run_ctl
	 set    rcp_run_no     = nvl(rcp_run_no,0) + 1,
	        UPD_USER_CODE  = :global.usercode,
          LAST_UPD_DATE  = sysdate
   where  iss_off_code   = :parameter.usr_off_code
   and    iss_br_code    = :parameter.usr_br_code
   and    sys_code       = '35'
   and    pc_no          = :blk12.pc_no; 
   exception when others then
   	   alert := f_show_alert(sqlerrm,'al_stop'); 
				clear_form(no_validate,full_rollback);
				raise form_trigger_failure;
   end;
   
   ------------generate key
   begin
   select rcp_run_no
   into   ln_Max
   from   fnc_rcp_run_ctl
   where  iss_off_code = :parameter.usr_off_code
   and    iss_br_code  = :parameter.usr_br_code
   and    sys_code     = '35'
   and    pc_no        = :blk12.pc_no;
   exception when no_data_found then
   	  ln_Max := 1;
   end;
   
   :blk12.rcp_no := trim(to_char(ln_Max));
   
   --check type à¾×èÍ Å§ car_circle_mas
   if (:blk12.type in ('12','17','91','92','93','94','88') and :parameter.new_bill_12_y50 in ('1','2') and substr(nvl(:blk1.DSP_EXP_DATE_NEW,:blk1.DSP_EXP_DATE),9,2) = '50') or
		  (:blk12.type in ('12','17','91','92','93','94','88') and :parameter.new_bill_12_y51 in ('1','2') and substr(nvl(:blk1.DSP_EXP_DATE_NEW,:blk1.DSP_EXP_DATE),9,2) = '51') or 
		 	(:blk12.type not in ('12','17','91','92','93','94','88') and :parameter.new_bill_y50 in ('1','2') and substr(nvl(:blk1.DSP_EXP_DATE_NEW,:blk1.DSP_EXP_DATE),9,2) = '50') or 
		 	(:blk12.type not in ('12','17','91','92','93','94','88') and :parameter.new_bill_y51 in ('1','2') and substr(nvl(:blk1.DSP_EXP_DATE_NEW,:blk1.DSP_EXP_DATE),9,2) = '51') or
		 	(:blk12.type in ('12','17','91','92','93','94','88') and :parameter.new_bill_12_y52 in ('1','2') and substr(nvl(:blk1.DSP_EXP_DATE_NEW,:blk1.DSP_EXP_DATE),9,2) = '52') or
		  (:blk12.type in ('12','17','91','92','93','94','88') and :parameter.new_bill_12_y53 in ('1','2') and substr(nvl(:blk1.DSP_EXP_DATE_NEW,:blk1.DSP_EXP_DATE),9,2) = '53') or 
		 	(:blk12.type not in ('12','17','91','92','93','94','88') and :parameter.new_bill_y52 in ('1','2') and substr(nvl(:blk1.DSP_EXP_DATE_NEW,:blk1.DSP_EXP_DATE),9,2) = '52') or 
		 	(:blk12.type not in ('12','17','91','92','93','94','88') and :parameter.new_bill_y53 in ('1','2') and substr(nvl(:blk1.DSP_EXP_DATE_NEW,:blk1.DSP_EXP_DATE),9,2) = '53') or
		 	substr(nvl(:blk1.DSP_EXP_DATE_NEW,:blk1.DSP_EXP_DATE),9,2) > '53' then
		 	 if :parameter.new_bill_12_y50 = '2' or :parameter.new_bill_12_y51 = '2' or 
			 		:parameter.new_bill_y50 = '2' or :parameter.new_bill_y51 = '2' or
			 		:parameter.new_bill_12_y52 = '2' or :parameter.new_bill_12_y53 = '2' or 
			 		:parameter.new_bill_y52 = '2' or :parameter.new_bill_y53 = '2' or
			 	substr(nvl(:blk1.DSP_EXP_DATE_NEW,:blk1.DSP_EXP_DATE),9,2) > '53' then
			 	ls_type := '5';
			 else
			 	ls_type := '4';
			 end if;
	 else
		   if :blk12.type in ('12','17','91','92','93','94','88') then
		   	ls_type := '2';--Ã¶¨Ñ¡ÃÂÒ¹Â¹µì
		   elsif :blk12.type in ('05','06','07','08','09','10','11') then
		   	ls_type := '3';--Ã¶ÊÒ¸ÒÃ³Ð
		   else
		   	ls_type := '1';--Ã¶Â¹µì
		   end if;
	 end if;   
   if :blk12.rdo_print = '2' then  -----¶éÒàÅ×Í¡ãºàÊÃç¨áººÁÕ»éÒÂÇ§¡ÅÁ 
   	  :blk12.TYPE_PRINT_FLAG := '1'; ----»éÒÂÇ§¡ÅÁ
   else	
     begin
		 select print_flag,form_type
		 into   ls_PrintFlag,ls_FormType
		 from   CTLTprint
		 where  IP_ADDR   = :parameter.ip_addr  
     and    OFF_CODE  = :parameter.usr_off_code       
     and    BR_CODE   = :parameter.usr_br_code;
	   exception when no_data_found then
	   	 ls_PrintFlag := null;
	   	 ls_FormType  := null;
	   end;
	   
	   if ls_PrintFlag = 'C' then
	   	  :blk12.TYPE_PRINT_FLAG := '3';   ------3 Copy
	   else
	   	  :blk12.TYPE_PRINT_FLAG := '2';   ------3 µÍ¹
	   end if;
	   
	 end if;	
	 
	 
	 if :blk12.rdo_print = '2' then  -----¶éÒàÅ×Í¡ãºàÊÃç¨áººÁÕ»éÒÂÇ§¡ÅÁ
		       begin
		       update car_mas
		   	   set    last_circ_no      = :blk12.RCP_SRL_NO,
		   	          last_rcp_no1      = :blk12.pc_no,
		   	          last_rcp_no2      = :blk12.rcp_no,
		   	          last_rcp_srl_no   = :blk12.rcp_srl_no,
		   	          UPD_USER_CODE     = :global.usercode,
		              LAST_UPD_DATE     = sysdate
		       where  off_code = :blk1.txt_off_code
		       and    br_code  = :blk1.txt_br_code  
		       and    type     = :blk12.type    
		       and    plate1   = :blk12.car_plate1
		       and    plate2   = :blk12.car_plate2;
		       exception when others then
						  alert := f_show_alert(sqlerrm,'al_stop'); 
							clear_form(no_validate,full_rollback);
							raise form_trigger_failure;    
		       end;
		       
		       if :blk1.dsp_exp_date_new is not null then
		          ls_Year := substr(:blk1.dsp_exp_date_new,7,4);
		       else
		       	  ls_Year := substr(:blk1.DSP_EXP_DATE,7,4);
		       end if;
		       
		       begin
		   	   insert into car_circle_mas(circ_type,CIRC_NO,circ_year,OFF_CODE,BR_CODE,TYPE,PLATE1,PLATE2                 
		                   ,TR_DATE,RCP_NO1,RCP_NO2,RCP_SRL_NO,ISS_OFF_CODE           
		                   ,ISS_BR_CODE,RFID_FLAG,UPD_USER_CODE,LAST_UPD_DATE,CREATE_USER_CODE       
		                   ,CREATE_DATE)
		               values (ls_type,:blk12.RCP_SRL_NO,ls_Year,:blk12.off_code,:blk12.car_br_code,
		                       :blk12.type,
		                       :blk12.car_plate1,:blk12.car_plate2,ls_TrDate,:blk12.pc_no,
		                       :blk12.rcp_no,:blk12.rcp_srl_no,:parameter.usr_off_code,
		                       :parameter.usr_br_code,:parameter.RFID_FLAG,:global.usercode,sysdate,
		                       :global.usercode,sysdate);
		       exception when others then
						  alert := f_show_alert(sqlerrm,'al_stop'); 
							clear_form(no_validate,full_rollback);
							raise form_trigger_failure;    
		       end;
		       
		   else
		   	 	 begin
		       update car_mas
		   	   set    last_rcp_no1      = :blk12.pc_no,
		   	          last_rcp_no2      = :blk12.rcp_no,
		   	          last_rcp_srl_no   = :blk12.rcp_srl_no,
		   	          UPD_USER_CODE     = :global.usercode,
		              LAST_UPD_DATE     = sysdate
		       where  off_code = :blk1.txt_off_code
		       and    br_code  = :blk1.txt_br_code  
		       and    type     = :blk12.type    
		       and    plate1   = :blk12.car_plate1
		       and    plate2   = :blk12.car_plate2;
		       exception when others then
						  alert := f_show_alert(sqlerrm,'al_stop'); 
							clear_form(no_validate,full_rollback);
							raise form_trigger_failure;    
		       end;
		end if;
		
-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (BEGIN)

if (NICEPLATE.pm_token is not null or
  NICEPLATE.pm_token2 is not null or
  NICEPLATE.pm_token3 is not null) then
  
  if(checkbox_checked('blk1.chk_chg_plate_flag') or
    checkbox_checked('blk1.chk_alter_flag') or
    checkbox_checked('blk1.chk_trf_flag') or
    (checkbox_checked('blk1.chk_imf_flag') and checkbox_checked('blk1.chk_addr_flag')) or
    checkbox_checked('blk1.chk_nouse_flag')) then

    NICEPLATE.OperationFlag := '0';
  
    if(checkbox_checked('blk1.chk_trf_flag')) then
      NICEPLATE.OperationFlag := NICEPLATE.OperationFlag||'1';
    else
      NICEPLATE.OperationFlag := NICEPLATE.OperationFlag||'0';
    end if;
    if(checkbox_checked('blk1.chk_chg_plate_flag')) then
      NICEPLATE.OperationFlag := NICEPLATE.OperationFlag||'1';
    else
      NICEPLATE.OperationFlag := NICEPLATE.OperationFlag||'0';
    end if;
    if(checkbox_checked('blk1.chk_alter_flag')) then
      NICEPLATE.OperationFlag := NICEPLATE.OperationFlag||'1';
    else
      NICEPLATE.OperationFlag := NICEPLATE.OperationFlag||'0';
    end if;
    if(checkbox_checked('blk1.chk_imf_flag') and checkbox_checked('blk1.chk_addr_flag')) then
      NICEPLATE.OperationFlag := NICEPLATE.OperationFlag||'1';
    else
      NICEPLATE.OperationFlag := NICEPLATE.OperationFlag||'0';
    end if;

    NICEPLATE.c_rcp_no := :blk12.rcp_no;
    NICEPLATE.p_update_receipt(:blk12.car_plate1,:blk12.car_plate2);

    if(NICEPLATE.att_but_printbid_name is not null) then
      set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_false);
    end if;
    if (NICEPLATE.pm_token is not null) then
      set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_true);
      NICEPLATE.pm_formlock := 1;
    end if;

    if (NICEPLATE.pm_token2 is not null) then
      set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_true);
      NICEPLATE.pm_formlock := 1;
    end if;

    if (NICEPLATE.pm_token3 is not null) then
      set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_true);
      NICEPLATE.pm_formlock := 1;
    end if;
  end if;

end if;

-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (END)

	--creditàÃÔèÁºÑµÃà¤Ã´Ôµ---------------------------------------
if :blk12.pay_with in ('4','7') then
		begin
			select vat_rate ,bank_acq_code,EDC_TIME_OUT
			into svat_rate ,sbank_acq_code,sEDC_TIME_OUT
			from ctlt_ebank ;
		exception when no_data_found then 	
			 svat_rate := 0;
			 sbank_acq_code := null;
			 sEDC_TIME_OUT := 0;
		end;
		begin
			select BANK_FEE_PERC
			into sBANK_FEE_PERC
			from tab_bank 
			where bank_code = sbank_acq_code ;
		exception when no_data_found then
			   sBANK_FEE_PERC := 0 ;
		end;
		begin
			select nvl(com_port,'X')
			into com_port
			from ctltip_addr
			where ip_addr = :parameter.ip_addr;
		exception 
			when no_data_found then
			com_port := 'X';
		end;
		if com_port = 'X' then
			alert := f_show_alert('COM PORT à¤Ã×èÍ§ÃÙ´ºÑµÃ ã¹ÃÐºº§Ò¹äÁè¶Ù¡µéÍ§','al_stop');
			--clear_form(no_validate,full_rollback);
			rollback;
			raise form_trigger_failure;
		end if;
--		param := 'BBL_EDC_SET_TIMEOUT~' || '60000' || ';'; --msec
   	param := 'BBL_EDC_SET_TIMEOUT~' ||to_char(nvl(sEDC_TIME_OUT,0))|| ';'; --msec
		retParam := CallNativeStr(hFld, param);
	--OPEN
		param := 'BBL_EDC_OPEN~' || com_port || ';';
		retParam := CallNativeStr(hFld, param);
		--message(retParam); message(' ');
		if retParam <> 'TRUE' then
			alert := f_show_alert('äÁèÊÒÁÒÃ¶ãªé§Ò¹à¤Ã×èÍ§ÃÙ´ºÑµÃä´é','al_stop');
			--clear_form(no_validate,full_rollback);
			rollback;
			raise form_trigger_failure;
		end if;
		
		
		--:TEXT_RESPONSE := retParam;		
	--send to EDC
  	ls_text_line1 := 'ÃÂ.'||:blk1.txt_type||' '||:blk12.car_plate1||' '||to_char(to_number(:blk12.car_plate2))||' '||'('||ls_OffRegAbrev||')';
		ls_text_line2 := :blk12.pc_no||' '||:parameter.user_code;
		--à§Ô¹Êè§ä»¸¹Ò¤ÒÃ = à§Ô¹ÀÒÉÕ + ¤èÒ¸ÃÃÁà¹ÕÂÁ1.5% + vat7%¨Ò¡¤èÒ¸ÃÃÁà¹ÕÂÁ
	--	ls_amt := :blk12.TXT_PAY_IN + ceil(:blk12.TXT_PAY_IN * 0.015) + ceil(ceil(:blk12.TXT_PAY_IN * 0.015)*0.07);
	  ls_amt := :blk12.TXT_PAY_IN + round((:blk12.TXT_PAY_IN * (nvl(sBANK_FEE_PERC,0)/100)),2) + round((round((:blk12.TXT_PAY_IN * (nvl(sBANK_FEE_PERC,0)/100)),2)*(svat_rate/100)),2);
		--param := 'BBL_EDC_SALE~' || :blk3.txt_receive_amt || '~' || ls_TEXT_LINE1 || '~' || ls_TEXT_LINE2 || ';';
		param := 'BBL_EDC_SALE~' || ls_amt || '~' || ls_TEXT_LINE1 || '~' || ls_TEXT_LINE2 || ';';
		retParam := CallNativeStr(hFld, param);
		
		param := 'BBL_EDC_RESPONSE;';
		retParam := CallNativeStr(hFld, param);
		resp_code := substr(retParam,9,2);
		--batch_no := substr(retParam,17,6);
		:blk12.credit_batch_no := substr(retParam,17,6);
		--trace_invoice := substr(retParam,23,6);
		:blk12.credit_system_trace := substr(retParam,11,6);
		:blk12.credit_trace_invoid := substr(retParam,23,6);
		--card_no := substr(retParam,47,20);
		:blk12.pay_no := substr(retParam,47,20);
		--terminal_id := substr(retParam,109,8);
		:blk12.credit_terminal_id := substr(retParam,109,8);
		begin
					 select nvl(bank_code,'00'),nvl(type_card,'1')
					 into ls_bank_code,ls_type_card
					 from tab_range_card
					 where CARD_DIGIT = substr(retParam,47,6);
				exception when no_data_found then
					ls_bank_code := '00';
					ls_type_card := '1';
				end;
				:blk12.BANK_CODE := ls_bank_code;
				if ls_type_card = '2' then
					:blk12.pay_with := '7';
				else
					:blk12.pay_with := '4';
				end if;
				
				if :blk12.bank_code is not null then
						begin
							select bank_name
							into  :blk12.dsp_bank_name
							from   tab_bank
							where  bank_code = :blk12.bank_code;
						exception when no_data_found then
							:blk12.dsp_bank_name := null;
						end;							
				else	
						:blk12.dsp_bank_name := null;		 
				end if;
		/*
		:blk12.credit_fee_amt := ceil(:blk12.TXT_PAY_IN * 0.015);--à§Ô¹ 1.5% --:blk3.txt_receive_amt*1.01;
		:blk12.credit_vat_amt := ceil(ceil(:blk12.TXT_PAY_IN * 0.015)*0.07);
		:blk12.total_bank_amt := :blk12.TXT_PAY_IN + ceil(:blk12.TXT_PAY_IN * 0.015) + ceil(ceil(:blk12.TXT_PAY_IN * 0.015)*0.07);*/
		:blk12.credit_fee_amt := round((:blk12.TXT_PAY_IN * (nvl(sBANK_FEE_PERC,0)/100)),2);--à§Ô¹ 1.5% 
		:blk12.credit_vat_amt := round((round((:blk12.TXT_PAY_IN * (nvl(sBANK_FEE_PERC,0)/100)),2)*(svat_rate/100)),2);
		:blk12.total_bank_amt := :blk12.TXT_PAY_IN + round((:blk12.TXT_PAY_IN * (nvl(sBANK_FEE_PERC,0)/100)),2) + round((round((:blk12.TXT_PAY_IN * (nvl(sBANK_FEE_PERC,0)/100)),2)*(svat_rate/100)),2);
		--message(retParam
		if nvl(resp_code,'xx') <> '00' then
			alert := f_show_alert('à¡Ô´¢éÍ¼Ô´¾ÅÒ´ã¹¡ÒÃãªéºÑµÃ','al_stop');
			--clear_form(no_validate,full_rollback);
			rollback;
			raise form_trigger_failure;
		end if;
		--message(str); message(' ');
		--:TEXT_RESPONSE := retParam;

	--CLOSE
		param := 'BBL_EDC_CLOSE;';  
		retParam := CallNativeStr(hFld, param);
		--:TEXT_RESPONSE := retParam;
end if; 
	 	   
  
   commit_form;
   --post;
  if :blk12.pay_with = '4' then
	--	ls_pay_with := '2';
	  ls_pay_with := '4';
	elsif :blk12.pay_with = '7' then
	  ls_pay_with := '7';
	else
		ls_pay_with := '1';
	end if;
   if :system.form_status = 'QUERY' then
   	   if ls_pay_with = '4' then
		   	   --------------update car_tax
		       begin
		   	   update  car_tax
		   	   set     rcp_no1       = :blk12.pc_no,
		   	           rcp_no2       = :blk12.RCP_NO,
		   	           rcp_srl_no    = :blk12.RCP_SRL_NO,
		   	           rcp_flag      = '1',
		   	           pay_doc_no    = substr(:blk12.pay_no,1,25),
		   	           iss_off_code  = :parameter.usr_off_code,
		   	           iss_br_code   = :parameter.usr_br_code,
		   	           pay_flag      = :blk12.pay_type,
		   	           UPD_USER_CODE = :global.usercode,
		               LAST_UPD_DATE = sysdate ,
		               pay_with = ls_pay_with ,
		               sub_pay_with = '41'
		       where   off_code = :blk1.txt_off_code
		       and     br_code  = :blk1.txt_br_code
		       and     type     = :blk12.type
		       and     plate1   = :blk12.car_plate1
		       and     plate2   = :blk12.car_plate2
		       and     rcp_flag = '0'
		       and     chg_type_flag <> '1';
		       exception when others then
						  alert := f_show_alert(sqlerrm,'al_stop'); 
							clear_form(no_validate,full_rollback);
							raise form_trigger_failure;          
		       end;
		       
		       -------------update car_hist_tax
		       begin
		       update  car_hist_tax
		       set     rcp_no1    = :blk12.pc_no,
		               rcp_no2    = :blk12.RCP_NO,
		               rcp_srl_no = :blk12.RCP_SRL_NO,
		               pay_flag   = :blk12.pay_type,
		               pay_doc_no = :blk12.PAY_NO,
		               pay_with = ls_pay_with ,sub_pay_with = '41'
		       where   off_code   = :blk1.txt_off_code
		       and     br_code    = :blk1.txt_br_code
		       and     type       = :blk12.type
		       and     plate1     = :blk12.car_plate1
		       and     plate2     = :blk12.car_plate2
		       and     tr_date   = ls_TrDate
		       and     rcp_no2    is null        
		       and     rcp_srl_no is null;
		       exception when others then
		       	  alert := f_show_alert(sqlerrm,'al_stop'); 
							clear_form(no_validate,full_rollback);
							raise form_trigger_failure;    
		       end;
     else
   	       --------------update car_tax
       begin
   	   update  car_tax
   	   set     rcp_no1       = :blk12.pc_no,
   	           rcp_no2       = :blk12.RCP_NO,
   	           rcp_srl_no    = :blk12.RCP_SRL_NO,
   	           rcp_flag      = '1',
   	           pay_doc_no    = substr(:blk12.pay_no,1,25),
   	           iss_off_code  = :parameter.usr_off_code,
   	           iss_br_code   = :parameter.usr_br_code,
   	           pay_flag      = :blk12.pay_type,
   	           UPD_USER_CODE = :global.usercode,
               LAST_UPD_DATE = sysdate ,
               pay_with = ls_pay_with
       where   off_code = :blk1.txt_off_code
       and     br_code  = :blk1.txt_br_code
       and     type     = :blk12.type
       and     plate1   = :blk12.car_plate1
       and     plate2   = :blk12.car_plate2
       and     rcp_flag = '0'
       and     chg_type_flag <> '1';
       exception when others then
				  alert := f_show_alert(sqlerrm,'al_stop'); 
					clear_form(no_validate,full_rollback);
					raise form_trigger_failure;          
       end;
       
       -------------update car_hist_tax
       begin
       update  car_hist_tax
       set     rcp_no1    = :blk12.pc_no,
               rcp_no2    = :blk12.RCP_NO,
               rcp_srl_no = :blk12.RCP_SRL_NO,
               pay_flag   = :blk12.pay_type,
               pay_doc_no = :blk12.PAY_NO,
               pay_with = ls_pay_with
       where   off_code   = :blk1.txt_off_code
       and     br_code    = :blk1.txt_br_code
       and     type       = :blk12.type
       and     plate1     = :blk12.car_plate1
       and     plate2     = :blk12.car_plate2
       and     tr_date   = ls_TrDate
       and     rcp_no2    is null        
       and     rcp_srl_no is null;
       exception when others then
       	  alert := f_show_alert(sqlerrm,'al_stop'); 
					clear_form(no_validate,full_rollback);
					raise form_trigger_failure;    
       end;
	  end if;		 	   
/*		   if :blk12.rdo_print = '2' then  -----¶éÒàÅ×Í¡ãºàÊÃç¨áººÁÕ»éÒÂÇ§¡ÅÁ
		       begin
		       update car_mas
		   	   set    last_circ_no      = :blk12.RCP_SRL_NO,
		   	          last_rcp_no1      = :blk12.pc_no,
		   	          last_rcp_no2      = :blk12.rcp_no,
		   	          last_rcp_srl_no   = :blk12.rcp_srl_no,
		   	          UPD_USER_CODE     = :global.usercode,
		              LAST_UPD_DATE     = sysdate
		       where  off_code = :blk1.txt_off_code
		       and    br_code  = :blk1.txt_br_code  
		       and    type     = :blk1.txt_type    
		       and    plate1   = :blk12.car_plate1
		       and    plate2   = :blk12.car_plate2;
		       exception when others then
						  message(sqlerrm);
						  rollback;
						  raise form_trigger_failure;    
		       end;
		       
		       if :blk1.dsp_exp_date_new is not null then
		          ls_Year := substr(:blk1.dsp_exp_date_new,7,4);
		       else
		       	  ls_Year := substr(:blk1.DSP_EXP_DATE,7,4);
		       end if;
		       
		       begin
		   	   insert into car_circle_mas(circ_type,CIRC_NO,circ_year,OFF_CODE,BR_CODE,TYPE,PLATE1,PLATE2                 
		                   ,TR_DATE,RCP_NO1,RCP_NO2,RCP_SRL_NO,ISS_OFF_CODE           
		                   ,ISS_BR_CODE,rfid_flag,UPD_USER_CODE,LAST_UPD_DATE,CREATE_USER_CODE       
		                   ,CREATE_DATE)
		               values (ls_type,:blk12.RCP_SRL_NO,ls_Year,:blk12.off_code,:blk12.car_br_code,
		                       :blk12.type,
		                       :blk12.car_plate1,:blk12.car_plate2,ls_TrDate,:blk12.pc_no,
		                       :blk12.rcp_no,:blk12.rcp_srl_no,:parameter.usr_off_code,
		                       :parameter.usr_br_code,:parameter.rfid_flag,:global.usercode,sysdate,
		                       :global.usercode,sysdate);
		       exception when others then
						  message(sqlerrm);
						  rollback;
						  raise form_trigger_failure;    
		       end;
		       
		   else
		   	 	 begin
		       update car_mas
		   	   set    last_rcp_no1      = :blk12.pc_no,
		   	          last_rcp_no2      = :blk12.rcp_no,
		   	          last_rcp_srl_no   = :blk12.rcp_srl_no,
		   	          UPD_USER_CODE     = :global.usercode,
		              LAST_UPD_DATE     = sysdate
		       where  off_code = :blk1.txt_off_code
		       and    br_code  = :blk1.txt_br_code  
		       and    type     = :blk1.txt_type    
		       and    plate1   = :blk12.car_plate1
		       and    plate2   = :blk12.car_plate2;
		       exception when others then
						  message(sqlerrm);
						  rollback;
						  raise form_trigger_failure;    
		       end;
			 end if;*/
		       	
  
       commit;
       --post;
 
      -----------áººã¢àÊÃç¨
			if :blk12.rdo_print = '2' then  -----¶éÒàÅ×Í¡ãºàÊÃç¨áººÁÕ»éÒÂÇ§¡ÅÁ 
				 -----µèÍÍÒÂØÀÒÉÕÃ¶,
				 -----ãºá·¹»éÒÂÇ§¡ÅÁ,á¨é§ãªéÃ¶,à»ÅÕèÂ¹àÅ¢·ÐàºÕÂ¹¡Ã³Õ¾ÔàÈÉ
			   --P_PRINT_LASER;
			   P_PRINT_LASER_NEW_DLL;
			   -----âÂ¹¤èÒàÅ¢»éÒÂÇ§¡ÅÁ
			   :blk12.txt_circle_no := :blk12.RCP_SRL_NO; 
			elsif :blk12.rdo_print = '1' then  -----¶éÒàÅ×Í¡ãºàÊÃç¨áºº 3 Copy   
			   --ls_PrintFlag := 'C';
				 if ls_PrintFlag = 'C' then
				    P_PRINT_CITOH_NEW_DLL;
				 else
				 	  if nvl(ls_FormType,'0') = '0' then
				 	     P_PRINT_BROTHER_3_COPY_NEW_DLL;
				 	  else
				 	  	 P_PRINTBROTHER3COPY_NEW_LAYOUT;
				 	  end if;
				 end if;
			end if;
			
			 
			 :global.RCP_SRL_NO := :blk12.RCP_SRL_NO;
			 	---------------Modify by Chetsada Purpose: Refresh Time------------
				P_REFRESH_TIME(:parameter.usr_off_code,
				               :parameter.usr_br_code,
				               :blk12.DSP_TIME);
				               
				ls_WhiteColor := get_item_property('blk1.txt_off_code',BACKGROUND_COLOR);
		    ls_GrayColor  := get_item_property('blk1.DSP_OFF_BR_DESC',BACKGROUND_COLOR);       
				-------------------End Modify by Chetsada--------------------------
				if :blk1.CHK_ALTER_FLAG = '1' then       ----ÊÅÑºàÅ¢·ÐàºÕÂ¹
					 if nvl(:blk_data.PRINT_SWAP_PLATE12,0) = 0 then
						 p_show_alert('al_note','¡ÃØ³ÒãÊèÃÒÂÅÐàÍÕÂ´ àÅ¢¤ØÁ ¢Í§ãºàÊÃç¨ãº¶Ñ´ä»');
			       P_DEFAULT_FNC_RCP(:blk1.txt_plate1,:blk1.txt_plate2,
			                         :blk12.car_plate1,:blk12.car_plate2,
			                         :blk12.type);
			       :blk_data.PRINT_SWAP_PLATE12 := 1;
			       
						 set_item_property('blk12.pc_no',insert_allowed,property_true);
						 set_item_property('blk12.pc_no',update_allowed,property_true);
						 set_item_property('blk12.pc_no',BACKGROUND_COLOR,ls_WhiteColor);			 
					 else
					 	 	set_item_property('blk12.pc_no',insert_allowed,property_false);
						  set_item_property('blk12.pc_no',update_allowed,property_false);
							set_item_property('blk12.pc_no',BACKGROUND_COLOR,ls_GrayColor);
					 end if;    
			  else
					 	set_item_property('blk12.pc_no',insert_allowed,property_false);
					  set_item_property('blk12.pc_no',update_allowed,property_false);
						set_item_property('blk12.pc_no',BACKGROUND_COLOR,ls_GrayColor);	
				end if;
				
				set_item_property('blk1.BUT_12',enabled,property_false);
				
	 else
	 	   	alert := f_show_alert(sqlerrm,'al_stop'); 
				clear_form(no_validate,full_rollback);
				raise form_trigger_failure;			
	 end if;

end;