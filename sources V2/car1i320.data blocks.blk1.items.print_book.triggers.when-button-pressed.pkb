﻿-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (BEGIN)

if(NICEPLATE.att_but_printbid_name is not null) then
  set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_false);
end if;
if (NICEPLATE.pm_token is not null) then
  set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_true);
  NICEPLATE.pm_formlock := 1;
end if;

if (NICEPLATE.pm_token2 is not null) then
  set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_true);
  NICEPLATE.pm_formlock := 1;
end if;

if (NICEPLATE.pm_token3 is not null) then
  set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_true);
  NICEPLATE.pm_formlock := 1;
end if;

-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (END)