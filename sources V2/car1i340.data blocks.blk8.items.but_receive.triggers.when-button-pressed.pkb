﻿-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (BEGIN)

if (NICEPLATE.pm_token is not null or
  NICEPLATE.pm_token2 is not null or
  NICEPLATE.pm_token3 is not null) then
  
  NICEPLATE.PM_PRINT_BILL_COUNT := 1;
  NICEPLATE.OperationFlag := '10000';
  NICEPLATE.c_rcp_no := :blk8.rcp_no;
  NICEPLATE.p_update_receipt();
  
  if(NICEPLATE.att_but_printbid_name is not null) then
    set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_false);
  end if;
  if (NICEPLATE.pm_token is not null) then
    set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_true);
    NICEPLATE.pm_formlock := 1;
  end if;
  
  if (NICEPLATE.pm_token2 is not null) then
    set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_true);
    NICEPLATE.pm_formlock := 1;
  end if;
  
  if (NICEPLATE.pm_token3 is not null) then
    set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_true);
    NICEPLATE.pm_formlock := 1;
  end if;

end if;

-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (END)