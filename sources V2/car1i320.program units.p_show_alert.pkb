﻿PROCEDURE P_SHOW_ALERT(ls_AlertType   in varchar2,
                       ls_Message     in varchar2) IS
/*Purpose : show alert-------------------
   ls_AlertType ex. al_stop,al_note
   ls_Message = message show */
     Alert    number;
BEGIN
     set_alert_property(ls_AlertType,alert_message_text,ls_Message);
     Alert := show_alert(ls_AlertType);   
END;