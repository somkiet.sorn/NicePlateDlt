﻿-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (BEGIN)

NICEPLATE.PM_INIT_FORMCODE := 'CAR1I030';
NICEPLATE.p_init_params();

NICEPLATE.att_off_code := :parameter.usr_off_Code;
NICEPLATE.att_br_code := :parameter.usr_br_Code;
NICEPLATE.att_pc_no := :blk1.txt_pc_no;
NICEPLATE.att_car_type := :blk1.txt_type;
NICEPLATE.att_user_code := :global.usercode;
NICEPLATE.att_but_printbid_name := 'blk1.but_printbid';
NICEPLATE.att_but_printbid_blk_name := 'blk1';

NICEPLATE.plate_chars3 := :blk8.txt_new_plate1;
NICEPLATE.plate_num3 := :blk8.txt_new_plate2;

NICEPLATE.QueryCurrentProvince;
NICEPLATE.QueryCurrentOfficer;

NICEPLATE.p_chk_lic(3);

-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (END)
