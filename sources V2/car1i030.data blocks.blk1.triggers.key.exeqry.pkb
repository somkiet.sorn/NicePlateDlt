﻿-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (BEGIN)

declare
  al_ret number;
begin
  NICEPLATE.PM_INIT_FORMCODE := 'CAR1I030';
  NICEPLATE.p_init_params();

  NICEPLATE.att_off_code := :parameter.usr_off_Code;
  NICEPLATE.att_br_code := :parameter.usr_br_Code;
  NICEPLATE.att_pc_no := :blk1.txt_pc_no;
  NICEPLATE.att_car_type := :blk1.txt_type;
  NICEPLATE.att_user_code := :global.usercode;
  NICEPLATE.att_but_printbid_name := 'blk1.but_printbid';
  NICEPLATE.att_but_printbid_blk_name := 'blk1';

  NICEPLATE.plate_chars := :blk1.txt_plate1;
  NICEPLATE.plate_num := :blk1.txt_plate2;
  NICEPLATE.phaseNumber := 1;

  NICEPLATE.QueryCurrentProvince;
  NICEPLATE.QueryCurrentOfficer;

  NICEPLATE.p_chk_lic(1);

  if(checkbox_checked('blk1.CHK_TRF_FLAG')) then
    al_ret := show_alert(find_alert('AL_TRANSFER'));
    if(al_ret = ALERT_BUTTON2) then
      NICEPLATE.PM_DONT_TRANSFER_PLATE := 1;
      set_alert_property('AL_LIC',ALERT_MESSAGE_TEXT,'¡ÃØ³ÒÊ§Ç¹ÊÔ·¸ÔàÅ¢·ÐàºÕÂ¹»ÃÐÁÙÅ¡èÍ¹s');
      al_ret := show_alert('AL_LIC');
    end if;
  end if;
end;

-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (END)