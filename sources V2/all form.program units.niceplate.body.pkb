﻿-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (BEGIN)

PACKAGE BODY NICEPLATE IS

PROCEDURE QueryPersonFromPlate(p_plate1 varchar2,p_plate2 varchar2) IS
  BEGIN

  owrEvidCode := null;
  owrEvidDesc := null;
  owrEvidProvCode := null;
  owrEvidAmphCode := null;
  owrEvidDocYear := null;
  owrEvidDocNo := null;
  owrSeq := null;
  owrDspPrvDesc := null;
  owrDspAmpDesc := null;
  owrTitleCode := null;
  owrTitleDesc := null;
  owrFirstName := null;
  owrLastName := null;
  owrBirthDate := null;
  owrNationalityCode := null;
  owrNationalityDesc := null;
  owrMobilePhone := null;
  owrAddress := null;
  owrDistCode := null;
  owrAmphCode := null;
  owrProvCode := null;
  owrProvAmphDistDesc := null;
  owrPostalCode := null;

  lglEvidCode :=null;
  lglEvidDesc :=null;
  lglEvidProvCode :=null;
  lglEvidAmphCode :=null;
  lglEvidDocYear :=null;
  lglEvidDocNo :=null;
  lglSeq :=null;
  lglDspPrvDesc :=null;
  lglDspAmpDesc :=null;
  lglTitleCode :=null;
  lglTitleDesc :=null;
  lglFirstName :=null;
  lglLastName :=null;
  lglBirthDate :=null;
  lglNationalityCode :=null;
  lglNationalityDesc :=null;
  lglMobilePhone :=null;
  lglAddress :=null;
  lglDistCode :=null;
  lglAmphCode :=null;
  lglProvCode :=null;
  lglProvAmphDistDesc :=null;
  lglPostalCode :=null;

  /*
  --ALL FIELD
  owrEvidCode,owrEvidDesc,owrEvidProvCode,owrEvidAmphCode,
  owrEvidDocYear,owrEvidDocNo,owrSeq,owrDspPrvDesc,owrDspAmpDesc,
  owrTitleCode,owrTitleDesc,owrFirstName,owrLastName,
  owrBirthDate,owrNationalityCode,owrNationalityDesc,owrMobilePhone,
  owrAddress,owrDistCode,owrAmphCode,owrProvCode,
  owrProvAmphDistDesc,owrPostalCode

  --MUST QUERY FIELD
  owrEvidDesc,
  owrDspPrvDesc,owrDspAmpDesc,
  owrTitleDesc,
  owrNationalityDesc,
  owrProvAmphDistDesc,
  */

  --query owner
  begin
    select
      PERSON_CODE,PRV_CODE,AMP_CODE,
      DOC_YEAR,DOC_NO,SEQ,
      TITLE_CODE,FNAME,LNAME,
      BIRTH_DATE,NAT_CODE,PHONE_MOBILE,
      ADDR,DIST_CODE,PERSON_AMP_CODE,PERSON_PRV_CODE,
      ZIP_CODE
    into
      owrEvidCode,owrEvidProvCode,owrEvidAmphCode,
      owrEvidDocYear,owrEvidDocNo,owrSeq,
      owrTitleCode,owrFirstName,owrLastName,
      owrBirthDate,owrNationalityCode,owrMobilePhone,
      owrAddress,owrDistCode,owrAmphCode,owrProvCode,
      owrPostalCode
    from car_per_combine
    where
      off_code = att_off_code and
      br_code  = att_br_code  and
      type     = att_car_type     and
      plate1   = p_plate1   and
      plate2   = p_plate2   and
      own_flag = '2';
  exception when no_data_found then
      owrEvidCode:=null;
      owrEvidProvCode:=null;
      owrEvidAmphCode:=null;
      owrEvidDocYear:=null;
      owrEvidDocNo:=null;
      owrSeq:=null;
      owrTitleCode:=null;
      owrFirstName:=null;
      owrLastName:=null;
      owrBirthDate:=null;
      owrNationalityCode:=null;
      owrMobilePhone:=null;
      owrAddress:=null;
      owrDistCode:=null;
      owrAmphCode:=null;
      owrProvCode:=null;
      owrPostalCode:=null;
  end;

  begin
    select doc_desc into owrEvidDesc from tab_doc
    where doc_type = to_char(to_number(owrEvidCode));
    exception when no_data_found then
      owrEvidCode := null;
      owrEvidDesc := null;
  end;

  begin
    select prv_desc into owrDspPrvDesc from tab_province
    where prv_code = owrEvidProvCode;
    exception when no_data_found then
      owrDspPrvDesc := null;
  end;

  begin
    select amp_desc into owrDspAmpDesc from tab_amphur
    where prv_code = owrEvidProvCode and amp_code = owrEvidAmphCode;
    exception when no_data_found then
      owrDspAmpDesc := null;
  end;

  begin
    select title_desc into owrTitleDesc from tab_title
    where title_code = owrTitleCode;
    exception when no_data_found then
      owrTitleCode := null;
      owrTitleDesc := null;
  end;

  begin
    select owrNationalityDesc into owrNationalityDesc from tab_national
    where nat_code = owrNationalityCode;
    exception when no_data_found then
      owrNationalityCode := null;
      owrNationalityDesc := null;
  end;
  owrProvAmphDistDesc := trim(owrDspPrvDesc)||' '||trim(owrDspAmpDesc);

  --query legal
  begin
    select
      PERSON_CODE,PRV_CODE,AMP_CODE,
      DOC_YEAR,DOC_NO,SEQ,
      TITLE_CODE,FNAME,LNAME,
      BIRTH_DATE,NAT_CODE,PHONE_MOBILE,
      ADDR,DIST_CODE,PERSON_AMP_CODE,PERSON_PRV_CODE,
      ZIP_CODE
    into
      lglEvidCode,lglEvidProvCode,lglEvidAmphCode,
      lglEvidDocYear,lglEvidDocNo,lglSeq,
      lglTitleCode,lglFirstName,lglLastName,
      lglBirthDate,lglNationalityCode,lglMobilePhone,
      lglAddress,lglDistCode,lglAmphCode,lglProvCode,
      lglPostalCode
    from car_per_combine
    where
      off_code = att_off_code and
      br_code  = att_br_code  and
      type     = att_car_type     and
      plate1   = p_plate1   and
      plate2   = p_plate2   and
      own_flag = '1';
  exception when no_data_found then
      lglEvidCode:=null;
      lglEvidProvCode:=null;
      lglEvidAmphCode:=null;
      lglEvidDocYear:=null;
      lglEvidDocNo:=null;
      lglSeq:=null;
      lglTitleCode:=null;
      lglFirstName:=null;
      lglLastName:=null;
      lglBirthDate:=null;
      lglNationalityCode:=null;
      lglMobilePhone:=null;
      lglAddress:=null;
      lglDistCode:=null;
      lglAmphCode:=null;
      lglProvCode:=null;
      lglPostalCode:=null;
  end;

  begin
    select doc_desc into lglEvidDesc from tab_doc
    where doc_type = to_char(to_number(lglEvidCode));
    exception when no_data_found then
      lglEvidCode := null;
      lglEvidDesc := null;
  end;

  begin
    select prv_desc into lglDspPrvDesc from tab_province
    where prv_code = lglEvidProvCode;
    exception when no_data_found then
      lglDspPrvDesc := null;
  end;

  begin
    select amp_desc into lglDspAmpDesc from tab_amphur
    where prv_code = lglEvidProvCode and amp_code = lglEvidAmphCode;
    exception when no_data_found then
      lglDspAmpDesc := null;
  end;

  begin
    select title_desc into lglTitleDesc from tab_title
    where title_code = lglTitleCode;
    exception when no_data_found then
      lglTitleCode := null;
      lglTitleDesc := null;
  end;

  begin
    select lglNationalityDesc into lglNationalityDesc from tab_national
    where nat_code = lglNationalityCode;
    exception when no_data_found then
      lglNationalityCode := null;
      lglNationalityDesc := null;
  end;
  lglProvAmphDistDesc := trim(lglDspPrvDesc)||' '||trim(lglDspAmpDesc);

END;

PROCEDURE QueryCurrentOfficer IS
  BEGIN

    officer_titleCode := null;
    officer_titleName := null;
    officer_fname := null;
    officer_lname := null;
  begin
    select nvl(b.title_desc,''),nvl(a.user_fname,''),nvl(a.user_lname,''),a.title_code
    into   officer_titlename,officer_fname,officer_lname,officer_titleCode
    from   ctltemp a,tab_title b
    where  a.title_code = b.title_code
      and    a.user_code  = att_user_code;
    exception when no_data_found then
      officer_titleCode := null;
      officer_titlename := null;
      officer_fname := null;
      officer_lname := null;
  end;

END;

PROCEDURE QueryCurrentProvince IS
  BEGIN

  licenseProvince := null;

  begin
    select prv_desc into licenseProvince from tab_province
    where prv_code = att_off_code;
    exception when no_data_found then
      licenseProvince := null;
  end;

END;

PROCEDURE QueryCarDataByPlate(p_plate1 varchar2,p_plate2 varchar2) IS
  BEGIN

    c_brn_code := null;
    c_brn_desc := null;
    c_status_code := null;
    c_hold_flag := null;
  c_num_body := null;
  begin
    select status_code,hold_flag,brn_code,num_body
    into c_status_code,c_hold_flag,c_brn_code,c_num_body
    from car_mas
    where  off_code = att_off_code
      and    br_code  = att_br_code
      and    type     = att_car_type
      and    plate1   = p_plate1
      and    plate2   = p_plate2;
    exception when no_data_found then
      c_brn_code := null;
      c_brn_desc := null;
      c_status_code := null;
      c_hold_flag := null;
    c_num_body := null;
  end;

  QueryCarBrandByCode(c_brn_code);
END;

PROCEDURE QueryCarBrandByCode(p_brn_code varchar2) IS
   BEGIN

    c_brn_desc := null;
  begin
    select brn_desc
    into   c_brn_desc
    from   tab_brand
    where  brn_code = p_brn_code;
    exception when no_data_found then
      c_brn_desc := null;
  end;

END;

PROCEDURE p_chk_lic (plate_set number) IS
  ex ora_java.jobject;
  al_id alert;
  al_ret number;

  plate_chars varchar2(3);
  plate_num varchar2(4);
  nice_plate_chars varchar2(3);
  nice_plate_num varchar2(4);
  att_token varchar2(3000);
  att_action_url varchar2(3000);

  qry_bid_flag char(1);
  qry_count number;
begin

  if (plate_set = 1) then
    plate_chars := NICEPLATE.plate_chars;
    plate_num := NICEPLATE.plate_num;
    nice_plate_chars := TRIM(NICEPLATE.plate_chars);
    nice_plate_num := TRIM(LEADING '0' FROM NICEPLATE.plate_num);
  elsif (plate_set = 2) then
    plate_chars := NICEPLATE.plate_chars2;
    plate_num := NICEPLATE.plate_num2;
    nice_plate_chars := TRIM(NICEPLATE.plate_chars2);
    nice_plate_num := TRIM(LEADING '0' FROM NICEPLATE.plate_num2);
  elsif (plate_set = 3) then
    plate_chars := NICEPLATE.plate_chars3;
    plate_num := NICEPLATE.plate_num3;
    nice_plate_chars := TRIM(NICEPLATE.plate_chars3);
    nice_plate_num := TRIM(LEADING '0' FROM NICEPLATE.plate_num3);
  end if;

  -- check is reserve plate
  begin
    select bid_flag into qry_bid_flag from mac_plate
    where off_code = NICEPLATE.att_off_code and
      plt_type = '1' and type = NICEPLATE.att_car_type and
      plate1 = plate_chars and plate2 = plate_num;
    exception when no_data_found then
      qry_bid_flag := 'N';
  end;
  if (qry_bid_flag is null or not qry_bid_flag = 'Y') then
    return;
  end if;

  -- check is certificate
  begin
    select count(*) into qry_count from CAR_NicePlate
    where Characters = nice_plate_chars and
      LicenseNumber = nice_plate_num and
      ProvinceName = NICEPLATE.licenseProvince and
      IsCancel = 0;
    exception when no_data_found then
      qry_count := 0;
  end;
  if (qry_count = 0) then
    al_id := find_alert('AL_LIC');
    set_alert_property(al_id,ALERT_MESSAGE_TEXT,' àÅ¢·ÐàºÕÂ¹¹ÕéÂÑ§äÁèÍÍ¡Ë¹Ñ§Ê×ÍÊÓ¤Ñ­áÊ´§ÊÔ·¸ÔÏ');
    al_ret := show_alert(al_id);
    return;
  end if;

  -- check is print all logs
  begin
    select count(*) into qry_count
    from DLT.CAR_OperationRequest
    where Characters = nice_plate_chars and
      LicenseNumber = nice_plate_num and
      ProvinceRefCode = NICEPLATE.att_off_code and
      IsDone = 1 and IsPrinted = 0;
    exception when no_data_found then
      qry_count := 0;
  end;

  -- check is print all logs for CAR1I150
  if (NICEPLATE.PM_INIT_FORMCODE = 'CAR1I150' and qry_count >= 0) then
    begin
      select Token into NICEPLATE.pm_token from car_operationrequest
      where Characters = nice_plate_chars and
      LicenseNumber = nice_plate_num and
      ProvinceRefCode = NICEPLATE.att_off_code and
      IsDone = 1 and IsPrinted = 0 and
      rownum = 1;
    exception when no_data_found then
      NICEPLATE.pm_token := null;
    end;
    if(NICEPLATE.pm_token is not null) then
      NICEPLATE.pm_print_url := NICEPLATE.PM_INIT_PRINT_URL || NICEPLATE.pm_token || '?chars=' || NICEPLATE.f_text_to_ascii(nice_plate_chars) || '&nums=' || nice_plate_num || '&procode=' || NICEPLATE.att_off_code || '&branchcode=' || NICEPLATE.att_br_code || '&formcode=' || NICEPLATE.PM_INIT_FORMCODE;
      set_item_property(NICEPLATE.att_but_printbid_name,ENABLED ,PROPERTY_TRUE);
      web.show_document(NICEPLATE.pm_print_url);
    else
      set_item_property(NICEPLATE.att_but_printbid_name,ENABLED ,PROPERTY_FALSE);
    end if;
    return;
  end if;

  if(NICEPLATE.PM_INIT_FORMCODE = 'CAR1I320') then
    begin
      select Token into att_token
      from DLT.CAR_OperationRequest
      where Characters = nice_plate_chars and
        LicenseNumber = nice_plate_num and
        ProvinceRefCode = NICEPLATE.att_off_code and
        FormCode = 'CAR1I300' and
        IsDone = 1 and IsPrinted = 0;
      exception when no_data_found then
        att_token := null;
    end;

    if(att_token = null) then
      al_id := find_alert('AL_LIC');
      set_alert_property(al_id,ALERT_MESSAGE_TEXT,'äÁè¾º¢éÍÁÙÅ');
      al_ret := show_alert(al_id);
      return;
    end if;
  else
    -- generate token
    att_token := null;
    begin
      SELECT SYS_GUID() into att_token FROM dual;
    end;
/*
    -- insert CAR_OperationRequest
    begin
      insert into CAR_OperationRequest
        (Id,Characters,LicenseNumber,
        ProvinceRefCode,ProvinceName,BranchCode,
        FormCode,PcCode,Token,
        PhaseNumber,OfficerCode,IsProceedAllowed,
        IsDone,ReceiptNumber,IsPrinted,
        NiceplateOperReqId,OperationFlag)
      values(0,nice_plate_chars,nice_plate_num,
        NICEPLATE.att_off_code,NICEPLATE.licenseProvince,NICEPLATE.att_br_code,
        NICEPLATE.PM_INIT_FORMCODE,NICEPLATE.att_pc_no,att_token,
        0,:global.usercode,0,0,null,0,null,'00000');
    end;*/
  end if;

  att_action_url := NICEPLATE.PM_INIT_CHECK_CERT_URL || att_token || '?chars=' || NICEPLATE.f_text_to_ascii(nice_plate_chars) || '&nums=' || nice_plate_num || '&procode=' || NICEPLATE.att_off_code || '&branchcode=' || NICEPLATE.att_br_code || '&formcode=' || NICEPLATE.PM_INIT_FORMCODE;

  set_item_property(NICEPLATE.att_but_printbid_name,ENABLED ,PROPERTY_FALSE);

  al_id := find_alert('AL_LIC');
  set_alert_property(al_id,ALERT_MESSAGE_TEXT,'¡ÃØ³Ò¡ÃÍ¡àÅ¢¤ØÁË¹Ñ§Ê×ÍÊÓ¤Ñ­áÊ´§ÊÔ·¸ÔÏ');
  al_ret := show_alert(al_id);

  web.show_document(att_action_url);

  al_id := find_alert('AL_LIC');
  set_alert_property(al_id,ALERT_MESSAGE_TEXT,'¡ÃÍ¡àÅ¢¤ØÁË¹Ñ§Ê×ÍÊÓ¤Ñ­áÊ´§ÊÔ·¸ÔÏ àÃÕÂºÃéÍÂáÅéÇ');
  al_ret := show_alert(al_id);

  if (plate_set = 1) then
    NICEPLATE.pm_token := att_token;
  elsif (plate_set = 2) then
    NICEPLATE.pm_token2 := att_token;
  elsif (plate_set = 3) then
    NICEPLATE.pm_token3 := att_token;
  end if;

  -- Check Proceed Allow Here
  NICEPLATE.p_chk_proceed(att_token);

exception
  when ORA_JAVA.JAVA_ERROR then
    message(' Unable to call out to Java, '||ORA_JAVA.LAST_ERROR );
  when ORA_JAVA.EXCEPTION_THROWN then
    ex := ORA_JAVA.LAST_EXCEPTION;
    message(OBJECT.toString(ex));
end;

PROCEDURE p_chk_proceed(p_token varchar2) IS
  ex ora_java.jobject;
  al_id alert;
  al_ret number;

  qry_IsProceesAllowed number;
begin
if (p_token is not null) then
  begin
    select IsProceedAllowed into qry_IsProceesAllowed
    from CAR_OperationRequest where Token = p_token;
    exception when no_data_found then
      qry_IsProceesAllowed := 0;
  end;
  if (qry_IsProceesAllowed = 0) then
    al_id := find_alert('AL_STOP');
    set_alert_property(al_id,ALERT_MESSAGE_TEXT,'àÅ¢¤ØÁË¹Ñ§Ê×ÍÊÓ¤Ñ­áÊ´§ÊÔ·¸ÔäÁè¶Ù¡µéÍ§ äÁèÊÒÁÒÃ¶µÓà¹Ô¹¡ÒÃµèÍä´é');
    al_ret := show_alert(al_id);
    -- clear form
    NICEPLATE.p_clear_variable;
    clear_form(no_validate,full_rollback);
    rollback;
  end if;
end if;

exception
  when ORA_JAVA.JAVA_ERROR then
    message(' Unable to call out to Java, '||ORA_JAVA.LAST_ERROR );
  when ORA_JAVA.EXCEPTION_THROWN then
    ex := ORA_JAVA.LAST_EXCEPTION;
    message(OBJECT.toString(ex));
end;

PROCEDURE p_update_receipt(plate1 varchar2,plate2 varchar2) IS
  ex ora_java.jobject;
	num varchar2(4);
begin
num := TRIM(LEADING '0' FROM plate2);
if (NICEPLATE.pm_token is not null or
  NICEPLATE.pm_token2 is not null or
  NICEPLATE.pm_token3 is not null) then
-- update CAR_OperationRequest
  begin
  update CAR_OperationRequest set
    FormCode = NICEPLATE.PM_INIT_FORMCODE,
    PcCode = NICEPLATE.att_pc_no,
    PhaseNumber = 1,
    OfficerCode = :global.usercode,
    IsDone = 1,
    OperationFlag = NICEPLATE.OperationFlag,
		ReceiptNumber = NICEPLATE.c_rcp_no
  where Characters = plate1 and LicenseNumber = num and Token in (NICEPLATE.pm_token, NICEPLATE.pm_token2, NICEPLATE.pm_token3);
  end;
  --commit;
end if;
exception
  when ORA_JAVA.JAVA_ERROR then
    message(' Unable to call out to Java, '||ORA_JAVA.LAST_ERROR );
  when ORA_JAVA.EXCEPTION_THROWN then
    ex := ORA_JAVA.LAST_EXCEPTION;
    message(OBJECT.toString(ex));
end;

PROCEDuRE p_init_params IS
begin
  PM_INIT_ADDITIONAL_DATA_URL := 'http://172.20.254.85/dltlinkservice/forms/extradata/';
  PM_INIT_CHECK_CERT_URL := 'http://172.20.254.85/dltlinkservice/forms/checkcertno/';
  PM_INIT_PRINT_URL := 'http://172.20.254.85/dltlinkservice/forms/paperprint2/';
  PM_INIT_CANCEL_URL := 'http://172.20.254.85/dltlinkservice/forms/cancelrequest/';
end;

PROCEDuRE p_clear_variable IS
begin

  if(NICEPLATE.att_but_printbid_name is not null) then
    go_block(NICEPLATE.att_but_printbid_blk_name);
    set_item_property(NICEPLATE.att_but_printbid_name,ENABLED ,PROPERTY_FALSE);
  end if;

  NICEPLATE.PM_FORMLOCK := null;
  NICEPLATE.PM_TOKEN := null;
  NICEPLATE.PM_TOKEN2 := null;
  NICEPLATE.PM_TOKEN3 := null;
  NICEPLATE.PM_ALERT_MSG := null;
  NICEPLATE.PM_ALERT_MSG2 := null;
  NICEPLATE.PM_ALERT_MSG3 := null;
  NICEPLATE.PM_ACTION_URL := null;
  NICEPLATE.PM_ACTION_URL2 := null;
  NICEPLATE.PM_ACTION_URL3 := null;
  NICEPLATE.PM_PRINT_URL := null;
  NICEPLATE.PM_PRINT_URL2 := null;
  NICEPLATE.PM_PRINT_URL3 := null;
  NICEPLATE.PM_PRINT_BILL_COUNT := 1;
  NICEPLATE.PM_DONT_TRANSFER_PLATE := 0;

  NICEPLATE.att_off_code := null;
  NICEPLATE.att_br_code := null;
  NICEPLATE.att_pc_no := null;
  NICEPLATE.att_car_type := null;
  NICEPLATE.att_user_code := null;
  NICEPLATE.att_but_printbid_name := null;

  NICEPLATE.plate_chars := null;
  NICEPLATE.plate_chars2 := null;
  NICEPLATE.plate_chars3 := null;
  NICEPLATE.plate_num := null;
  NICEPLATE.plate_num2 := null;
  NICEPLATE.plate_num3 := null;

  NICEPLATE.licenseProvince := null;

  NICEPLATE.officer_titleCode := null;
  NICEPLATE.officer_titleName := null;
  NICEPLATE.officer_fname := null;
  NICEPLATE.officer_lname := null;

  NICEPLATE.c_rcp_no := null;
  NICEPLATE.c_brn_code := null;
  NICEPLATE.c_brn_desc := null;
  NICEPLATE.c_status_code := null;
  NICEPLATE.c_hold_flag := null;
  NICEPLATE.c_num_body := null;

  NICEPLATE.owrEvidCode := null;
  NICEPLATE.owrEvidDesc := null;
  NICEPLATE.owrEvidProvCode := null;
  NICEPLATE.owrEvidAmphCode := null;
  NICEPLATE.owrEvidDocYear := null;
  NICEPLATE.owrEvidDocNo := null;
  NICEPLATE.owrSeq := null;
  NICEPLATE.owrDspPrvDesc := null;
  NICEPLATE.owrDspAmpDesc := null;
  NICEPLATE.owrTitleCode := null;
  NICEPLATE.owrTitleDesc := null;
  NICEPLATE.owrFirstName := null;
  NICEPLATE.owrLastName := null;
  NICEPLATE.owrBirthDate := null;
  NICEPLATE.owrNationalityCode := null;
  NICEPLATE.owrNationalityDesc := null;
  NICEPLATE.owrMobilePhone := null;
  NICEPLATE.owrAddress := null;
  NICEPLATE.owrDistCode := null;
  NICEPLATE.owrAmphCode := null;
  NICEPLATE.owrProvCode := null;
  NICEPLATE.owrProvAmphDistDesc := null;
  NICEPLATE.owrPostalCode := null;

  NICEPLATE.lglEvidCode := null;
  NICEPLATE.lglEvidDesc := null;
  NICEPLATE.lglEvidProvCode := null;
  NICEPLATE.lglEvidAmphCode := null;
  NICEPLATE.lglEvidDocYear := null;
  NICEPLATE.lglEvidDocNo := null;
  NICEPLATE.lglSeq := null;
  NICEPLATE.lglDspPrvDesc := null;
  NICEPLATE.lglDspAmpDesc := null;
  NICEPLATE.lglTitleCode := null;
  NICEPLATE.lglTitleDesc := null;
  NICEPLATE.lglFirstName := null;
  NICEPLATE.lglLastName := null;
  NICEPLATE.lglBirthDate := null;
  NICEPLATE.lglNationalityCode := null;
  NICEPLATE.lglNationalityDesc := null;
  NICEPLATE.lglMobilePhone := null;
  NICEPLATE.lglAddress := null;
  NICEPLATE.lglDistCode := null;
  NICEPLATE.lglAmphCode := null;
  NICEPLATE.lglProvCode := null;
  NICEPLATE.lglProvAmphDistDesc := null;
  NICEPLATE.lglPostalCode := null;
end;

FUNCTION f_text_to_ascii(text in varchar2) RETURN varchar2 IS
  ex ora_java.jobject;
  
  charStr char(1);
  charAscii number;
  charArrayAsTxt varchar2(3000);
BEGIN  
  for i in 1 .. length(text)
  loop
    charStr := substr(text, i, 1 );
    charAscii := ASCII(charStr);
    if(not i=1) then
      charArrayAsTxt := charArrayAsTxt||',';
    end if;
    charArrayAsTxt := charArrayAsTxt||charAscii;
  end loop;
  return charArrayAsTxt;

exception
  when ORA_JAVA.JAVA_ERROR then
    message(' Unable to call out to Java, '||ORA_JAVA.LAST_ERROR );
  when ORA_JAVA.EXCEPTION_THROWN then
    ex := ORA_JAVA.LAST_EXCEPTION;
    message(OBJECT.toString(ex));
end;

END;

-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (END)