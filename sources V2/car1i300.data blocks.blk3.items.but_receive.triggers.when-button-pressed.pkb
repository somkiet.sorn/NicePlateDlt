﻿-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (BEGIN)

if (NICEPLATE.pm_token is not null or
  NICEPLATE.pm_token2 is not null or
  NICEPLATE.pm_token3 is not null) then

  NICEPLATE.PM_PRINT_BILL_COUNT := 1;
  NICEPLATE.OperationFlag := '10000';
  NICEPLATE.c_rcp_no := :blk3.rcp_no;
  NICEPLATE.p_update_receipt();

end if;

-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (END)