﻿-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (BEGIN)

if (NICEPLATE.pm_token is not null or
  NICEPLATE.pm_token2 is not null or
  NICEPLATE.pm_token3 is not null) then
  
  if(checkbox_checked('blk1.chk_chg_plate_flag') or
    checkbox_checked('blk1.chk_alter_flag') or
    checkbox_checked('blk1.chk_trf_flag') or
    (checkbox_checked('blk1.chk_imf_flag') and checkbox_checked('blk1.chk_addr_flag')) or
    checkbox_checked('blk1.chk_nouse_flag')) then

    NICEPLATE.OperationFlag := '0';
  
    if(checkbox_checked('blk1.chk_trf_flag')) then
      NICEPLATE.OperationFlag := NICEPLATE.OperationFlag||'1';
    else
      NICEPLATE.OperationFlag := NICEPLATE.OperationFlag||'0';
    end if;
    if(checkbox_checked('blk1.chk_chg_plate_flag')) then
      NICEPLATE.OperationFlag := NICEPLATE.OperationFlag||'1';
    else
      NICEPLATE.OperationFlag := NICEPLATE.OperationFlag||'0';
    end if;
    if(checkbox_checked('blk1.chk_alter_flag')) then
      NICEPLATE.OperationFlag := NICEPLATE.OperationFlag||'1';
    else
      NICEPLATE.OperationFlag := NICEPLATE.OperationFlag||'0';
    end if;
    if(checkbox_checked('blk1.chk_imf_flag') and checkbox_checked('blk1.chk_addr_flag')) then
      NICEPLATE.OperationFlag := NICEPLATE.OperationFlag||'1';
    else
      NICEPLATE.OperationFlag := NICEPLATE.OperationFlag||'0';
    end if;

    NICEPLATE.c_rcp_no := :blk12.rcp_no;
    NICEPLATE.p_update_receipt(:blk12.car_plate1,:blk12.car_plate2);

    if(NICEPLATE.att_but_printbid_name is not null) then
      set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_false);
    end if;
    if (NICEPLATE.pm_token is not null) then
      set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_true);
      NICEPLATE.pm_formlock := 1;
    end if;

    if (NICEPLATE.pm_token2 is not null) then
      set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_true);
      NICEPLATE.pm_formlock := 1;
    end if;

    if (NICEPLATE.pm_token3 is not null) then
      set_item_property(NICEPLATE.att_but_printbid_name,enabled,property_true);
      NICEPLATE.pm_formlock := 1;
    end if;
  end if;

end if;

-- NICE PLATE CERTIFICATE WEBSERVICE PART 1/1 (END)